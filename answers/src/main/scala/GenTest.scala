import fpinscala.state.RNG
import fpinscala.testing.Gen
import fpinscala.testing.Prop

object GenTest extends App {
      val rng = RNG.simple(99)
      val listSize = 50
      val testCases = 50
	  val gen2 = Gen.choose(1, 100)
      val intList = Gen.listOfN(listSize, gen2)
      val prop = Prop.forAll(intList) { l =>
        println(l)
        l.reverse.reverse == l
      }
      println("starting")
      println(prop.run(1, testCases, rng))
}