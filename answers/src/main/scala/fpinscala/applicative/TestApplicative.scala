package fpinscala.applicative

object TestApplicative extends App {
  import Traverse.listTraverse

  implicit val listApplicative = new Applicative[List] {
    def unit[A](a: => A) = List(a)
    override def map[A, B](fa: List[A])(f: A => B): List[B] = fa.map { f }
    override def map2[A, B, C](fa: List[A], fb: List[B])(f: (A, B) => C): List[C] =
      (fa.zip(fb)).map { case (a, b) => f(a, b) }
  }

  if (false) {
    println(listTraverse.traverse[List, Int, Int](List(1))(x => List(x)))
    println(listTraverse.traverse[List, Int, Int](List(1, 2))(x => List(x)))
    println(listTraverse.traverse[List, Int, Int](List(1, 2, 3))(x => List(x)))
  //  def traverseS[S,A,B](fa: F[A])(f: A => State[S, B]): State[S, F[B]] =
 
  println(Monad.stateMonad[String].unit(3).run(""))
  val m3 = Monad.stateMonad[String].unit(3)
  val x3 = Monad.stateMonad[String].flatMap(m3)(x => Monad.stateMonad[String].unit(x + 1))
  println(x3.run(""))
  
  import Monad.stateMonad
  import fpinscala.state.State
  listTraverse.traverseS[Unit, Int, Int](List(1))(x => stateMonad.unit(x)).run(())
  //            traverse[({type f[x] = State[S, x]})#f, A, B](fa)(f)(Monad.stateMonad)
  (listTraverse.traverse[({type f[x] = State[Unit, x]})#f, Int, Int](List(1))(x => stateMonad.unit(x))(stateMonad)).run(())
  //as.foldRight(M.unit(List[B]()))((a, fbs) => M.map2(f(a), fbs)(_ :: _))
  List(1).foldRight(stateMonad[Unit].unit(List[Int]())){(a, fbs) =>
    val f = (x:Int) => stateMonad[Unit].unit(x)
    stateMonad.map2(f(a), fbs)(_ :: _)
  }
  // map2 =   apply(map(fa)(f.curried))(fb)
  List(1).foldRight(stateMonad[Unit].unit(List[Int]())){(a, fbs) =>
    val f = (x:Int) => stateMonad[Unit].unit(x)
    def f2(a: Int): List[Int] => List[Int] = { as => a :: as }
    stateMonad.apply(stateMonad.map(f(a))(f2 _))(fbs)
  }
  // map = apply(unit(f))(fa)
  List(1).foldRight(stateMonad[Unit].unit(List[Int]())){(a, fbs) =>
    val f = (x:Int) => stateMonad[Unit].unit(x)
    def f2(a: Int): List[Int] => List[Int] = { as => a :: as }
    stateMonad[Unit].apply(stateMonad[Unit].apply(stateMonad[Unit].unit(f2 _))(f(a)))(fbs)
  }
  // XXX Ok, that's the problem: 
  //     map is defined in terms of apply and vice versa
  // apply = flatMap(mf)(f => map(ma)(a => f(a)))
  }
  
  //println(listTraverse.traverseS[Unit, Int, Int](List(1))(x => Monad.stateMonad.unit(x)).run(()))
  //println(listTraverse.toList(List(1)))
  println(listTraverse.reverse(List(1,2,3)))
}