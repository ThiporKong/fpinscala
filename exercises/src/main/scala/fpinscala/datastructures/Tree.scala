package fpinscala.datastructures

sealed trait Tree[+A]
case class Leaf[A](value: A) extends Tree[A]
case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]

object Tree {
  //ex25
  def size[A](t: Tree[A]): Int = t match {
    case Leaf(_) => 1
    case Branch(l, r) => size(l) + size(r)
  }

  //ex26
  def max(t: Tree[Int]): Int = t match {
    case Leaf(value) => value
    case Branch(l, r) => max(l) max max(r)
  }

  //ex27
  def depth[A](t: Tree[A]): Int = t match {
    case Leaf(value) => 1
    case Branch(l, r) => depth(l) max depth(r)
  }

  //ex28
  def map[A, B](t: Tree[A])(f: A => B): Tree[B] = t match {
    case Leaf(value) => Leaf(f(value))
    case Branch(l, r) => Branch(map(l)(f), map(r)(f))
  }

  //ex29
  def fold[A, C](t: Tree[A])(f: (A) => C)(g: (C, C) => C): C = t match {
    case Leaf(value) => f(value)
    case Branch(l, r) => g(fold(l)(f)(g), fold(l)(f)(g))
  }

  object ex29 {
    def size[A](t: Tree[A]): Int = fold(t) { _ => 1 } { (bl, br) => bl + br }
    // problem: should use Option here
    def max(t: Tree[Int]): Int = fold(t) { (a) => a } { (bl, br) => bl max br }
    def depth[A](t: Tree[A]): Int = fold(t) { (_) => 1 } { (bl, br) => (bl max br) + 1 }
    def map[A, B](t: Tree[A])(f: A => B): Tree[B] =
      fold(t) { a => Leaf(f(a)): Tree[B] } { (cl: Tree[B], cr: Tree[B]) => Branch(cl, cr) }
  }
}