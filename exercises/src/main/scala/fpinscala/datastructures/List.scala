package fpinscala.datastructures

import scala.annotation.tailrec

sealed trait List[+A] // `List` data type
case object Nil extends List[Nothing] // data constructor for `List`
case class Cons[+A](head: A, tail: List[A]) extends List[A]

object List { // `List` companion object
  def sum(ints: List[Int]): Int = ints match { // Pattern matching example
    case Nil => 0
    case Cons(x, xs) => x + sum(xs)
  }

  def product(ds: List[Double]): Double = ds match {
    case Nil => 1.0
    case Cons(0.0, _) => 0.0
    case Cons(x, xs) => x * product(xs)
  }

  def apply[A](as: A*): List[A] = // Variadic function syntax
    if (as.isEmpty) Nil
    else Cons(as.head, apply(as.tail: _*))

  val example = Cons(1, Cons(2, Cons(3, Nil))) // Creating lists
  val example2 = List(1, 2, 3)
  val total = sum(example)

  val x = List(1, 2, 3, 4, 5) match {
    case Cons(x, Cons(2, Cons(4, _))) => x
    case Nil => 42
    case Cons(x, Cons(y, Cons(3, Cons(4, _)))) => x + y
    case Cons(h, t) => h + sum(t)
    case _ => 101
  }

  def append[A](a1: List[A], a2: List[A]): List[A] =
    a1 match {
      case Nil => a2
      case Cons(h, t) => Cons(h, append(t, a2))
    }

  def foldRight[A, B](l: List[A], z: B)(f: (A, B) => B): B = // Utility functions
    l match {
      case Nil => z
      case Cons(x, xs) => f(x, foldRight(xs, z)(f))
    }

  def sum2(l: List[Int]) =
    foldRight(l, 0.0)(_ + _)

  def product2(l: List[Double]) =
    foldRight(l, 1.0)(_ * _)

  // ex2
  def tail[A](l: List[A]): List[A] =
    l match {
      case Nil => Nil
      case Cons(hd, tl) => tl
    }

  // ex3
  @tailrec
  def drop[A](l: List[A], n: Int): List[A] =
    (l, n) match {
      case (_, n) if n <= 0 => l
      case (Nil, _) => Nil
      case (Cons(hd, tl), n) if n > 0 => drop(tl, n - 1)
    }

  // ex4
  @tailrec
  def dropWhile[A](l: List[A])(f: A => Boolean): List[A] =
    l match {
      case Cons(hd, tl) if f(hd) => dropWhile(tl)(f)
      case _ => l
    }

  //ex6
  def setHead[A](l: List[A])(h: A): List[A] =
    l match {
      case Cons(_, tl) => Cons(h, tl)
      case Nil => Nil
    }

  //ex6
  def init[A](l: List[A]): List[A] = {
    @tailrec
    def reverse(l: List[A], done: List[A] = Nil): List[A] = l match {
      case Cons(hd, tl) => reverse(tl, Cons(hd, done))
      case Nil => done
    }
    reverse(tail(reverse(l)))
  }

  // ex9
  def length[A](l: List[A]): Int = foldLeft(l, 0)((x, _) => x + 1)

  // ex10
  def foldLeft[A, B](l: List[A], z: B)(f: (B, A) => B): B =
    l match {
      case Nil => z
      case Cons(hd, tl) => foldLeft(tl, f(z, hd))(f)
    }

    // ex11
  object ex11 {
    def length[A](l: List[A]) = foldLeft(l, 0)((x, _) => x + 1)
    def sum[A](l: List[Int]) = foldLeft(l, 0)((x, a) => x + a)
    def product[A](l: List[Int]) = foldLeft(l, 1)((x, a) => x * a)
  }
  
  // ex12
  def reverse[A](l: List[A]) = foldLeft(l, Nil: List[A])((acc, a) => Cons(a, acc))

  object ex13 {
    def foldRight[A, B](l: List[A], z: B)(f: (A, B) => B): B =
      foldLeft(reverse(l), z)((b, a) => f(a, b))

    def length[A](l: List[A]) = foldRight(l, 0) { (a, x) => x + 1 }

    def foldLeft[A, B](l: List[A], z: B)(f: (B, A) => B): B = {
      foldRight(l, (l, z)) {
        case (_, (Cons(hd, tl), acc)) => (tl, f(acc, hd))
        case (_, (Nil, acc)) => (Nil, z)
      }._2
    }

    def sum[A](l: List[Int]) = foldLeft(l, 0) { (x, a) =>
      println(x, a)
      x + a
    }
  }

  object ex14 {
    def append[A](l1: List[A], l2: List[A]) = foldRight(l1, l2)(Cons(_, _))
  }

  // ex15
  def concat[A](ls: List[List[A]]): List[A] =
    foldLeft(ls, Nil: List[A])(append _)

  // ex16
  def add1(l: List[Int]) = map(l) { _ + 1 }

  // ex17
  def double2string(l: List[Double]) = map(l) { _.toString }

  // ex18
  def map[A, B](l: List[A])(f: A => B): List[B] =
    l match {
      case Cons(hd, tl) => Cons(f(hd), map(tl)(f))
      case Nil => Nil
    }

  // ex19
  def filter[A](l: List[A])(p: A => Boolean): List[A] =
    foldRight(l, Nil: List[A]) { (x, acc) => if (p(x)) Cons(x, acc) else acc }

  // ex20
  def flatMap[A, B](l: List[A])(f: A => List[B]): List[B] = concat(map(l)(f))
  flatMap(List(1, 2, 3)) { i => List(i, i) }

  object ex21 {
    def filter[A](l: List[A])(p: A => Boolean): List[A] =
      flatMap(l) { a => if (p(a)) List(a) else Nil }
  }

  // ex22
  def add(l1: List[Int], l2: List[Int]) = zip(l1, l2) { _ + _ }

  // ex23
  def zip[A, B, C](as: List[A], bs: List[B])(f: (A, B) => C): List[C] =
    (as, bs) match {
      case (Cons(a, atl), Cons(b, btl)) => Cons(f(a, b), zip(atl, btl)(f))
      case (Nil, Nil) => Nil
      case _ => sys.error("lengths differ")
    }

  // ex24
  @scala.annotation.tailrec
  def hasSubsequence[A](l: List[A], sub: List[A]): Boolean = {
    @scala.annotation.tailrec
    def isPrefix(l: List[A], sub: List[A]): Boolean =
      (l, sub) match {
        case (_, Nil) => true
        case (Cons(x, tl), Cons(xsub, tlsub)) if x == xsub => isPrefix(tl, tlsub)
        case _ => false
      }

    (l, sub) match {
      case (_, Nil) => true
      case (Nil, _) => false
      case (Cons(_, tl), _) => isPrefix(l, sub) || hasSubsequence(tl, sub)
    }
  }
}