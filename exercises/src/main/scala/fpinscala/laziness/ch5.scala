package fpscala.laziness.ch5

import scala.annotation.tailrec

trait Stream[+A] {
  def uncons: Option[(A, Stream[A])]
  def isEmpty: Boolean = uncons.isEmpty
  def toList: List[A]
  def take(n: Int): Stream[A]
  def takeWhile(p: A => Boolean): Stream[A]

  def foldRight[B](z: => B)(f: (A, => B) => B): B =
    uncons match {
      case Some((h, t)) => f(h, t.foldRight(z)(f))
      case None => z
    }
  def scanRight[B](z: => B)(f: (A, => B) => B): Stream[B] =
    uncons match {
      case Some((h, t)) => {
        val rest = t.scanRight(z)(f)
        rest.uncons match {
          case Some((hr, tr)) => Stream.cons(f(h, hr), rest)
        }
      }
      case None => Stream(z)
    }

  def exists(p: A => Boolean): Boolean =
    foldRight(false)((a, b) => p(a) || b)
  def forAll(p: A => Boolean): Boolean =
    foldRight(true)((a, b) => p(a) && b)

  def takeWhile2(p: A => Boolean): Stream[A] =
    foldRight(Stream.empty: Stream[A])((a, b) => if (p(a)) Stream.cons(a, b) else Stream.empty)

  def map[B](f: A => B): Stream[B] = foldRight(Stream.empty: Stream[B])((a, b) => Stream.cons(f(a), b))
  def flatMap[B](f: A => Stream[B]): Stream[B] = foldRight(Stream.empty: Stream[B])((a, b) => f(a).append(b))
  def filter(f: A => Boolean): Stream[A] = foldRight(Stream.empty: Stream[A])((a, b) => if (f(a)) Stream.cons(a, b) else b)
  def append[AA >: A](bs: Stream[AA]): Stream[AA] = foldRight(bs)((a, b) => Stream.cons(a, b))
}

object Stream {
  def empty[A]: Stream[A] = new Stream[A] {
    override def uncons = None
    override def toList = Nil
    override def take(n: Int): Stream[A] = empty
    override def takeWhile(p: A => Boolean): Stream[A] = empty
  }

  def unapply[A](s: Stream[A]) = s.uncons
  // why is tl also by-name? forcing the Stream shouldn't change anything, right?
  def cons[A](hd: => A, tl: => Stream[A]): Stream[A] =
    //def cons[A](hd: => A, tl: Stream[A]): Stream[A] =
    new Stream[A] {
      lazy val uncons = Some((hd, tl))
      override def toList = hd :: tl.toList
      override def take(n: Int): Stream[A] =
        if (n > 0) { cons(hd, tl.take(n - 1)) } else { empty } // if tl would'nt be by-name, the take() would force the stream
      override def takeWhile(p: A => Boolean): Stream[A] = {
        lazy val hdv = hd
        if (p(hdv)) { cons(hdv, tl.takeWhile(p)) } else { empty }
      }
    }
  def apply[A](as: A*): Stream[A] =
    if (as.isEmpty) empty
    else cons(as.head, apply(as.tail: _*))
}

object ex1 {
  import Stream._
  val s = cons({ println("1"); 1 }, cons({ println("2"); 2 }, cons({ println("3"); 3 }, cons({ println("4"); 4 }, empty))))
  println(s);
  println(s.toList);
}

object ex2 {
  import Stream._
  val s = cons({ println("1"); 1 }, cons({ println("2"); 2 }, cons({ println("3"); 3 }, cons({ println("4"); 4 }, empty))))
  val extractedLocalValue = s.take(3)
  println("done");
  println(extractedLocalValue.toList);
}

object ex3 {
  import Stream._
  val s = cons({ println("1"); 1 }, cons({ println("2"); 2 }, cons({ println("3"); 3 }, cons({ println("4"); 4 }, empty))))
  val extractedLocalValue = s.takeWhile(_ < 3)
  println("done");
  println(extractedLocalValue.toList);
}

object ex4 {
  import Stream._
  val s = cons({ println("1"); 1 }, cons({ println("2"); 2 }, cons({ println("3"); 3 }, cons({ println("4"); 4 }, empty))))
  val extractedLocalValue = s.forAll(_ < 2)
  println("done");
  println(extractedLocalValue);
}

object ex5 {
  import Stream._
  val s = cons({ println("1"); 1 }, cons({ println("2"); 2 }, cons({ println("3"); 3 }, cons({ println("4"); 4 }, empty))))
  val extractedLocalValue = s.takeWhile2(_ < 3)
  println("done");
  println(extractedLocalValue.toList);
}

object ex6 {
  import Stream._
  val s = cons({ println("s1"); 1 }, cons({ println("s2"); 2 }, cons({ println("s3"); 3 }, cons({ println("s4"); 4 }, empty))))
  val t = cons({ println("t1"); 11 }, cons({ println("t2"); 12 }, cons({ println("t3"); 13 }, cons({ println("t4"); 14 }, empty))))

  //  val st = s.append(t).map(_ * 2)
  val st = s.append(t).map(_ * 2)
  println("done");
  println(st.take(6).toList);
}

object ex7 {
  def constant[A](a: A): Stream[A] = Stream.cons(a, constant(a))
  val x = constant({ println("7"); 7 })
  println("done");
  println(x.take(6).toList);
}

object ex8 {
  def from(n: Int): Stream[Int] = Stream.cons(n, from(n + 1))
  val x = from(3)
  println("done");
  println(x.take(6).toList);

}

object ex9 {
  def fibs: Stream[Int] = {
    def fibs(a: Int, b: Int): Stream[Int] = Stream.cons(b, fibs(b, a + b))
    Stream.cons(0, fibs(0, 1))
  }
  val x = fibs
  println("done");
  println(x.take(6).toList);
}

object ex10 {
  def unfold[A, S](z: S)(f: S => Option[(A, S)]): Stream[A] =
    f(z) match {
      case Some((na, nz)) => Stream.cons(na, unfold(nz)(f))
      case None => Stream.empty
    }
}

object ex11 {
  import ex10.unfold
  def fibs = Stream.cons(0, unfold((0, 1)) { case (a, b) => Some((b, (b, a + b))) })
  def from(n: Int) = unfold(n) { n => Some((n, n + 1)) }
  def constant(n: Int) = unfold(n) { n => Some((n, n)) }
  def ones = constant(1)

  println(fibs.take(6))
  println(fibs.take(6).toList)
  println(from(5).take(6).toList)
}

object ex12 {
  import ex10.unfold
  import ex11.fibs

  def map[A, B](as: Stream[A])(f: A => B): Stream[B] =
    unfold(as) { case Stream(hd, tl) => Some(f(hd), tl) }
  def take[A](as: Stream[A], n: Int): Stream[A] =
    unfold((as, n)) { case (Stream(hd, tl), n) => if (n > 0) Some((hd, (tl, n - 1))) else None }
  def takeWhile[A](as: Stream[A])(p: A => Boolean): Stream[A] =
    unfold(as) { case Stream(hd, tl) => if (p(hd)) Some((hd, tl)) else None }
  def zip[A, B, C](as: Stream[A], bs: Stream[B])(f: (A, B) => C): Stream[C] =
    unfold((as, bs)) {
      case ((a, b)) => (a.uncons, b.uncons) match {
        case (Some((ahd, atl)), Some((bhd, btl))) => Some((f(ahd, bhd), (atl, btl)))
        case (_, None) => None
        case (None, _) => None
      }
    }

  def zipAll[A, B, C](as: Stream[A], bs: Stream[B])(f: (A, B) => C): Stream[Option[C]] =
    unfold((as, bs)) {
      case ((a, b)) => (a.uncons, b.uncons) match {
        case (Some((ahd, atl)), Some((bhd, btl))) => Some((Some(f(ahd, bhd)), (atl, btl)))
        case (Some((_, atl)), None) => Some((None, (atl, b)))
        case (None, Some((_, btl))) => Some((None, (a, btl)))
        case (None, None) => None
      }
    }

  println(take(map(fibs) { _ * 2 }, 6).toList)
}

object ex13 {
  //  @tailrec
  //  def startsWith[A](s: Stream[A], sub: Stream[A]): Boolean = (s.uncons, sub.uncons) match {
  //    case (_, None) => true
  //    case (Some((shd, stl)), Some((subhd, subtl))) if shd == subhd => startsWith(stl, subtl)
  //    case _ => false
  //  }
  import ex12._
  def startsWith[A](s: Stream[A], sub: Stream[A]): Boolean =
    (zipAll(s, sub)) { _ == _ }.forAll { x =>
      x match {
        case Some(eq) => eq
        case None => false
      }
    }

  @scala.annotation.tailrec
  def isSubsequence[A](s: Stream[A], sub: Stream[A]): Boolean = {
    startsWith(s, sub) || {
      (s.uncons) match {
        case Some((hd, tl)) => isSubsequence(tl, sub)
        case None => false
      }
    }
  }
  isSubsequence(Stream(1, 2, 3), Stream(1, 2, 3))
}

object ex14 {
  // def unfold[A, S](z: S)(f: S => Option[(A, S)]): Stream[A]
  import ex10.unfold
  def tails[A](s: Stream[A]): Stream[Stream[A]] =
    Stream.cons(s, unfold(s) { s =>
      s.uncons match {
        case Some((hd, tl)) => Some((tl, tl))
        case None => None
      }
    })
  tails(Stream(1, 2, 3)).map { _.toList }.toList

  import ex13.startsWith
  def hasSubsequence[A](s1: Stream[A], s2: Stream[A]): Boolean = tails(s1) exists (startsWith(_, s2))
  hasSubsequence(Stream(1, 2, 3), Stream(1, 3))
}

object ex15 {
  Stream(1,2,3).scanRight(0) { _ + _ }.toList
}