package fpinscala.laziness

import Stream.empty
import Stream.cons

trait Stream[+A] {
  def uncons: Option[(A, Stream[A])]
  def isEmpty: Boolean = uncons.isEmpty
  
  def foldRight[B](z: => B)(f: (A, => B) => B): B =
    uncons match {
      case Some((h, t)) => f(h, t.foldRight(z)(f))
      case None => z
    }

  def exists(p: A => Boolean): Boolean =
    foldRight(false)((a, b) => p(a) || b)

  def take(n: Int): Stream[A] = (uncons, n) match {
    case (None, _) => empty
    case (_, 0) => empty
    case (Some((hd, tl)), n) if n > 0 => cons(hd, tl.take(n - 1))
  }

  def takeWhile(p: A => Boolean): Stream[A] = uncons match {
    case None => empty
    case Some((hd, tl)) if p(hd) => cons(hd, tl.takeWhile(p))
  }

  def takeWhile2(p: A => Boolean): Stream[A] =
    foldRight(Stream.empty: Stream[A])((a, b) => if (p(a)) Stream.cons(a, b) else Stream.empty)

  def forAll(p: A => Boolean): Boolean =
    foldRight(true)((a, b) => p(a) && b)

  def toList: List[A] = uncons match {
    case Some((hd, tl)) => hd :: tl.toList
    case None => Nil
  }

  def map[B](f: A => B): Stream[B] = foldRight(Stream.empty: Stream[B])((a, b) => Stream.cons(f(a), b))
  
  def map2[B,C](b: Stream[B])(f: (A,B) => C): Stream[C] =
    (uncons, b.uncons) match {
	    case (Some((ahd,atl)), Some((bhd,btl))) => Stream.cons(f(ahd,bhd), atl.map2(btl)(f))
	    case _ => Stream.empty
  }
  
  def flatMap[B](f: A => Stream[B]): Stream[B] = foldRight(Stream.empty: Stream[B])((a, b) => f(a).append(b))
  def filter(f: A => Boolean): Stream[A] = foldRight(Stream.empty: Stream[A])((a, b) => if (f(a)) Stream.cons(a, b) else b)
  def append[AA >: A](bs: Stream[AA]): Stream[AA] = foldRight(bs)((a, b) => Stream.cons(a, b))

  def scanRight[B](z: => B)(f: (A, => B) => B): Stream[B] =
    uncons match {
      case Some((h, t)) => {
        val rest = t.scanRight(z)(f)
        rest.uncons match {
          case Some((hr, tr)) => Stream.cons(f(h, hr), rest)
          case None => Stream.empty
        }
      }
      case None => Stream(z)
    }
}

object Stream {
  def empty[A]: Stream[A] =
    new Stream[A] { def uncons = None }

  def cons[A](hd: => A, tl: => Stream[A]): Stream[A] =
    new Stream[A] {
      lazy val uncons = Some((hd, tl))
    }

  def apply[A](as: A*): Stream[A] =
    if (as.isEmpty) empty
    else cons(as.head, apply(as.tail: _*))

  val ones: Stream[Int] = cons(1, ones)
  
  def constant[A](a: A): Stream[A] = ex7.constant(a)
  def unit[A](a: A) = constant(a)
  
  def from(n: Int): Stream[Int] = ex8.from(n)

  def unfold[A, S](z: S)(f: S => Option[(A, S)]): Stream[A] = ex10.unfold(z)(f)

  def startsWith[A](s: Stream[A], s2: Stream[A]): Boolean = ex13.startsWith(s, s2)

  object ex1 {
    val s = cons({ println("1"); 1 }, cons({ println("2"); 2 }, cons({ println("3"); 3 }, cons({ println("4"); 4 }, empty))))
    println(s);
    println(s.toList);
  }

  object ex2 {
    val s = cons({ println("1"); 1 }, cons({ println("2"); 2 }, cons({ println("3"); 3 }, cons({ println("4"); 4 }, empty))))
    val taken = s.take(3)
    println("done");
    println(taken.toList);
  }

  object ex3 {
    val s = cons({ println("1"); 1 }, cons({ println("2"); 2 }, cons({ println("3"); 3 }, cons({ println("4"); 4 }, empty))))
    val taken = s.takeWhile(_ < 3)
    println("done");
    println(taken.toList);
  }

  object ex4 {
    val s = cons({ println("1"); 1 }, cons({ println("2"); 2 }, cons({ println("3"); 3 }, cons({ println("4"); 4 }, empty))))
    val extractedLocalValue = s.forAll(_ < 2)
    println("done");
    println(extractedLocalValue);
  }

  object ex5 {
    val s = cons({ println("1"); 1 }, cons({ println("2"); 2 }, cons({ println("3"); 3 }, cons({ println("4"); 4 }, empty))))
    val extractedLocalValue = s.takeWhile2(_ < 3)
    println("done");
    println(extractedLocalValue.toList);
  }

  object ex6 {
    val s = cons({ println("s1"); 1 }, cons({ println("s2"); 2 }, cons({ println("s3"); 3 }, cons({ println("s4"); 4 }, empty))))
    val t = cons({ println("t1"); 11 }, cons({ println("t2"); 12 }, cons({ println("t3"); 13 }, cons({ println("t4"); 14 }, empty))))

    //  val st = s.append(t).map(_ * 2)
    val st = s.append(t).map(_ * 2)
    println("done");
    println(st.take(6).toList);
  }

  object ex7 {
    def constant[A](a: A): Stream[A] = Stream.cons(a, constant(a))
    val x = constant({ println("7"); 7 })
    println("done");
    println(x.take(6).toList);
  }

  object ex8 {
    def from(n: Int): Stream[Int] = Stream.cons(n, from(n + 1))
    val x = from(3)
    println("done");
    println(x.take(6).toList);

  }

  object ex9 {
    def fibs: Stream[Int] = {
      def fibs(a: Int, b: Int): Stream[Int] = Stream.cons(b, fibs(b, a + b))
      Stream.cons(0, fibs(0, 1))
    }
    val x = fibs
    println("done");
    println(x.take(6).toList);
  }

  object ex10 {
    def unfold[A, S](z: S)(f: S => Option[(A, S)]): Stream[A] =
      f(z) match {
        case Some((na, nz)) => Stream.cons(na, unfold(nz)(f))
        case None => Stream.empty
      }
  }

  object ex11 {
    def fibs = Stream.cons(0, unfold((0, 1)) { case (a, b) => Some((b, (b, a + b))) })
    def from(n: Int) = unfold(n) { n => Some((n, n + 1)) }
    def constant(n: Int) = unfold(n) { n => Some((n, n)) }
    def ones = constant(1)

    println(fibs.take(6))
    println(fibs.take(6).toList)
    println(from(5).take(6).toList)
  }

  object ex12 {
    import ex11.fibs

    def map[A, B](as: Stream[A])(f: A => B): Stream[B] =
      unfold(as) {
        _.uncons match {
          case Some((hd, tl)) => Some(f(hd), tl)
          case None => None
        }
      }
    def take[A](as: Stream[A], n: Int): Stream[A] =
      unfold((as, n)) {
        case (s, n) => (s.uncons, n) match {
          case (Some((hd, tl)), n) => if (n > 0) Some((hd, (tl, n - 1))) else None
          case (None, _) => None
        }
      }
    def takeWhile[A](as: Stream[A])(p: A => Boolean): Stream[A] =
      unfold(as) {
        _.uncons match {
          case Some((hd, tl)) => if (p(hd)) Some((hd, tl)) else None
          case None => None
        }
      }
    def zip[A, B, C](as: Stream[A], bs: Stream[B])(f: (A, B) => C): Stream[C] =
      unfold((as, bs)) {
        case ((a, b)) => (a.uncons, b.uncons) match {
          case (Some((ahd, atl)), Some((bhd, btl))) => Some((f(ahd, bhd), (atl, btl)))
          case (_, None) => None
          case (None, _) => None
        }
      }

    def zipAll[A, B, C](as: Stream[A], bs: Stream[B])(f: (A, B) => C): Stream[Option[C]] =
      unfold((as, bs)) {
        case ((a, b)) => (a.uncons, b.uncons) match {
          case (Some((ahd, atl)), Some((bhd, btl))) => Some((Some(f(ahd, bhd)), (atl, btl)))
          case (Some((_, atl)), None) => Some((None, (atl, b)))
          case (None, Some((_, btl))) => Some((None, (a, btl)))
          case (None, None) => None
        }
      }

    println(take(map(fibs) { _ * 2 }, 6).toList)
  }

  object ex13 {
    //  @tailrec
    //  def startsWith[A](s: Stream[A], sub: Stream[A]): Boolean = (s.uncons, sub.uncons) match {
    //    case (_, None) => true
    //    case (Some((shd, stl)), Some((subhd, subtl))) if shd == subhd => startsWith(stl, subtl)
    //    case _ => false
    //  }
    import ex12._
    def startsWith[A](s: Stream[A], sub: Stream[A]): Boolean =
      (zipAll(s, sub)) { _ == _ }.forAll { x =>
        x match {
          case Some(eq) => eq
          case None => false
        }
      }

    @scala.annotation.tailrec
    def isSubsequence[A](s: Stream[A], sub: Stream[A]): Boolean = {
      startsWith(s, sub) || {
        (s.uncons) match {
          case Some((hd, tl)) => isSubsequence(tl, sub)
          case None => false
        }
      }
    }
    isSubsequence(Stream(1, 2, 3), Stream(1, 2, 3))
  }

  object ex14 {
    // def unfold[A, S](z: S)(f: S => Option[(A, S)]): Stream[A]
    def tails[A](s: Stream[A]): Stream[Stream[A]] =
      Stream.cons(s, unfold(s) { s =>
        s.uncons match {
          case Some((hd, tl)) => Some((tl, tl))
          case None => None
        }
      })
    tails(Stream(1, 2, 3)).map { _.toList }.toList

    def hasSubsequence[A](s1: Stream[A], s2: Stream[A]): Boolean = tails(s1) exists (startsWith(_, s2))
    hasSubsequence(Stream(1, 2, 3), Stream(1, 3))
  }

  object ex15 {
    Stream(1, 2, 3).scanRight(0) { _ + _ }.toList
  }
}