package fpinscala.testing.ch8

import fpinscala.state.RNG

// ex1
// sum(Nil) == 0
// sum(List(hd,tl)) == hd + sum(tl)

// ex2
// l.forall { _ <= l.max }

object ex3 {
  type MaxSize = Int
  type TestCases = Int
  type SuccessCount = Int
  type FailedCase = String
  type Result = Either[FailedCase, (Status, SuccessCount)]

  case class Prop(run: (MaxSize, TestCases, RNG) => Result) {
    def check(max: Int, n: TestCases, rng: RNG): Result = run(max, n, rng)
    // ex3/ex12
    def &&(p: Prop): Prop = {
      val run: (MaxSize, TestCases, RNG) => Result = (max, n, rng) => {
        this.run(max, n, rng) match {
          case r @ Left(value) => r
          case r @ Right(value) => p.run(max, n, rng)
        }
      }
      Prop { run }
    }
    // ex12
    def ||(p: Prop): Prop = {
      val run: (MaxSize, TestCases, RNG) => Result = (max, n, rng) => {
        this.run(max, n, rng) match {
          case r @ Left(value) => p.run(max, n, rng)
          case r @ Right(value) => r
        }
      }
      Prop { run }
    }
  }

  object Prop {
    def check(p: => Boolean): Prop = forAll(Gen.unit(()))(_ => p)

    //ex17
    def forAll[A](sgen: SGen[A])(f: A => Boolean): Prop =
      sgen match {
        case Sized(forSize) => forAll(forSize)(f)
        case Unsized(gen) => forAll(gen)(f)
      }

    def forAll[A](g: Int => Gen[A])(f: A => Boolean): Prop = {
      val run: (MaxSize, TestCases, RNG) => Result = (max, n, rng) => {
        // why is here a toList in the book?
        val casesPerSize = n / max + 1
        val props = Stream.from(0).take(max + 1).map { i => forAll(g(i))(f) }

        if (false) {
          // why do they recommend this complicated way? (even with converting the Stream to a List
          val results = props.map { p =>
            val run2 = (max: MaxSize, n: TestCases, rng: RNG) => p.run(max, casesPerSize, rng)
            Prop(run2)
          }
          results.reduceLeft(_ && _).run(max, n, rng)
        } else {
          props.reduceLeft(_ && _).run(-1, casesPerSize, rng)
        }
      }
      Prop(run)
    }

    def forAll[A](gen: Gen[A])(f: A => Boolean): Prop = {
      val run: (MaxSize, TestCases, RNG) => Result = (max, n, rng) => {
        def loop(todo: Int, rng: RNG): (Result, RNG) = {
          val (a, rng2) = gen.sample.run(rng)
          val success = f(a)
          if (success) {
            if (todo <= 1) {
              (Right(Status.Unfalsified, 1), rng2)
            } else {
              loop(todo - 1, rng2)
            }
          } else {
            (Left(a.toString), rng2)
          }
        }
        loop(n, rng)._1
      }
      Prop(run)
    }
  }
}

trait Status {}

object Status {
  case object Exhausted extends Status
  case object Proven extends Status
  case object Unfalsified extends Status
}

/*
object Gen {
  def unit[A](a: => A): Gen[A] = ???
}

trait Gen[A] {
  def map[A, B](f: A => B): Gen[B] = ???
  def flatMap[A, B](f: A => Gen[B]): Gen[B] = ???
}
*/

object ex4 {
  import fpinscala.state._
  type Gen[A] = State[RNG, A]

  def choose(start: Int, stopExclusive: Int): Gen[Int] =
    new Gen[Int]({ rng =>
      val (i, rng2) = RNG.positiveMax(stopExclusive - start)(rng)
      (start + i, rng2)
    })
}

import fpinscala.state._
import fpinscala.laziness._

case class Gen[+A](sample: State[RNG, A], exhaustive: Stream[Option[A]]) {

  // ex13
  def unsized: SGen[A] = Unsized(this)

  // ex7
  def map[B](f: A => B): Gen[B] = Gen(sample.map(f), exhaustive.map(_.map(f)))

  def map2[B, C](g: Gen[B])(f: (A, B) => C): Gen[C] =
    Gen(sample.map2(g.sample)(f), exhaustive.map2(g.exhaustive)(
      // is there a simple lift for 'f'?
      (a, b) => (a, b) match {
        case (Some(a), Some(b)) => Some(f(a, b))
        case _ => None
      }))

  // ex8
  def flatMap[B](f: A => Gen[B]): Gen[B] =
    Gen(sample.flatMap(f(_).sample),
      exhaustive.flatMap { _.map { f(_).exhaustive }.getOrElse(Stream(None)) })
  // if Gen[A] has signaled None, we just say, that domain can't be enumerated

  def listOfN(size: Gen[Int]): Gen[List[A]] =
    size.flatMap(n => ex5.listOfN(n, this))
}

object Gen {
  def unit[A](a: A) = Unsized(Gen(State.unit(a), Stream.empty))

  // ex15
  def listOf[A](g: Gen[A]): SGen[List[A]] = Sized(ex5.listOfN(_, g))
  // ex19
  def listOf1[A](g: Gen[A]): SGen[List[A]] = Sized(n => ex5.listOfN(n + 1, g))
}

//ex16
trait SGen[+A]
case class Sized[+A](forSize: Int => Gen[A]) extends SGen[A]
case class Unsized[+A](get: Gen[A]) extends SGen[A]

object ex5 {
  // ex8
  def sameParity(from: Int, to: Int): Gen[(Int, Int)] = {
    def makeSameParity(i: Int, j: Int) =
      (i, if ((i % 2) == (j % 2)) j else if (j > from) j - 1 else j + 1)
    choose(from, to).map2(choose(from, to))((i, j) => makeSameParity(i, j))
  }

  val rngBoolean = RNG.map(RNG.int) { _ % 2 == 0 }

  // ex10
  // type parameter was missing
  def union[A](g1: Gen[A], g2: Gen[A]): Gen[A] =
    Gen(State(rngBoolean).flatMap(if (_) g1.sample else g2.sample),
      g1.exhaustive.append(g2.exhaustive))

  // ex11
  //EXERCISE 11 (hard, optional): Implement weighted, a version of union
  //which accepts a weight for each Gen and generates values from each Gen with
  //probability proportional to its weight.
  // type parameter was missing
  def weighted[A](g1: (Gen[A], Double), g2: (Gen[A], Double)): Gen[A] = {
    def weightedExhaustive[A](g1: (Gen[A], Double), g2: (Gen[A], Double)): Stream[Option[A]] =
      Stream(None) // lazy!

    Gen(State(RNG.double).flatMap {
      uni => if (uni * (g1._2 + g2._2) <= g1._2) g1._1.sample else g2._1.sample
    }, weightedExhaustive(g1, g2))
  }

  // ex28

  // ex5/ex6
  def unit[A](a: => A): Gen[A] = Gen(State(RNG.unit(a)), Stream.unit(Some(a)))

  // ex5/ex6
  def boolean: Gen[Boolean] = Gen(
    State({ rng => val (i, rng2) = rng.nextInt; (i % 2 == 0, rng2) }),
    Stream(Some(true), Some(false)))

  // ex5/ex6
  def choose(start: Int, stopExclusive: Int): Gen[Int] = Gen(
    State({ rng =>
      val (i, rng2) = rng.nextInt;
      (Range(start, stopExclusive)(i.abs % (stopExclusive - start)), rng2)
    }),
    Stream(Range(start, stopExclusive).map { Some(_) }: _*))

  // ex5/ex6
  def listOfN[A](n: Int, g: Gen[A]): Gen[List[A]] = Gen(
    State({ rng =>
      val list = List.fill(n)(0).foldLeft((Nil: List[A], rng)) {
        case ((l, rng), _) =>
          val (a, rng2) = g.sample.run(rng)
          (a :: l, rng2)
      }
      (list._1.reverse, list._2)
    }),
    // None means this domain can't be (exhaustively) enumerated
    Stream(None))

  object ex9 {
    def listOfN[A](n: Int, g: Gen[A]): Gen[List[A]] = {
      Gen(State.sequence(List.fill(n)(g.sample)), Stream(None))
    }
  }
}