package fpinscala.testing

import fpinscala.testing.ch8._
import fpinscala.state.RNG
import fpinscala.parallelism.Par
import java.util.concurrent.Executors

object ch8test extends App {
  val rng = RNG.simple(4711)
  if (false) {
    println(RNG.sequence(List.fill(100)(ex4.choose(1, 100).run))(rng))

    // need to append None?
    println(ex5.boolean.exhaustive.toList)
    println(RNG.sequence(List.fill(100)(ex5.boolean.sample.run))(rng))

    val gen2 = ex5.choose(1, 100)
    println(gen2.exhaustive)
    println(RNG.sequence(List.fill(10)(gen2.sample.run))(rng))

    val gen3 = ex5.listOfN(10, gen2)
    println(gen3.exhaustive)
    println(gen3.sample.run(rng))
    println(RNG.sequence(List.fill(10)(gen3.sample.run))(rng))

    println(RNG.sequence(List.fill(10)(ex5.sameParity(-10, 10).sample.run))(rng))
  }

  if (false) {
    val gen2 = ex5.choose(1, 100)
    val gen3 = ex5.listOfN(10, gen2)
    println(ex3.Prop.forAll(gen3)({
      x =>
        println("checking " + x)
        x.size > 0
    }).check(10, 10, rng))
  }

  if (false) {
    val smallInt = ex5.choose(-10, 10)
    val maxProp = ex3.Prop.forAll(fpinscala.testing.ch8.Gen.listOf1(smallInt)) { l =>
      println("test " + l)
      val max = l.max
      !l.exists(_ > max)
    }
    println(maxProp.run(10, 40, rng))
  }

  if (false) {
    val ES = Executors.newCachedThreadPool()
    println(ex3.Prop.check({
      val p = Par.map(Par.unit(1))(_ + 1)
      val p2 = Par.unit(2)
      p(ES).get == p2(ES).get
    }).run(5, 10, rng))
  }
  
  if (true) {
    val rng = RNG.simple(99)
      val listSize = 50
      val testCases = 50
	  val gen2 = ex5.choose(1, 100)
      val intList = ex5.listOfN(listSize, gen2)
      val prop = ex3.Prop.forAll(intList) { l =>
        println(l)
        l.reverse.reverse == l
      }
      println(prop.run(10, testCases, rng))
  }
}