package fpinscala.iomonad

import java.util.concurrent.Executors
import java.util.concurrent.Callable
import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.TimeUnit

object ch13_5_2 extends App {
  import ch13_5._
  import ch13_5.xxx._
  import ch13_5.tramp._

  sealed trait Future[+A] {
    import Future._

    //    // ex10 XXX impossible to solve
    def listen(cb: A => Trampoline[Unit]): Unit =
      this.step match {
        case BindMore(_, _) => sys.error("should have been resolved by step")
        case More(_) => sys.error("should have been resolved by step")

        case Now(a) => cb(a)
        case Async(onFinish) => onFinish(cb)
        case BindAsync(onFinish, g) => {
          //def delay[A](a: => A): Trampoline[A] = More(() => Done(a))
          //onFinish(x => Trampoline.delay(g(x)) map (_ listen cb))
          onFinish({ x =>
            // aahh.. we're calling 'g' here, but that could be delayed!!!
            val gx: Future[A] = g(x)

            // how do we resolve this future gx?
            if (true) {
              if (false) {
                // (a) use trampoline
                val dgx: Trampoline[Future[A]] = if (true) {
                  // (a1)
                  tramp.More(() => tramp.Done(gx))
                } else {
                  // (a2) would be equivalent to (b) because of the following map()
                  Done(gx)
                }
                monadTrampoline.map(dgx)(future2 => future2.listen(cb))
              } else {
                // equivalent rewrite of (a1)
                if (true) {
                  tramp.Bind({ () => tramp.Done(gx) }, {
                    future2: Future[A] => tramp.Done(future2.listen(cb))
                  })
                } else
                if (true) {
                  // basically equivalent to (b)???
                  // gx && cb in f
                  tramp.Bind({ () => tramp.Done(()) }, {
                    future2: Unit => tramp.Done(gx.listen(cb))
                  })
	            } else {
                  // gx && cb in force
                  // basically equivalent to (b)???
                  tramp.Bind({ () => tramp.Done(gx.listen(cb)) }, {
                    future2: Unit => tramp.Done(future2)
                  })
                }
              }
            } else {
              // (b) don't use trampoline
              Done(gx.listen(cb))
            }
          })
        }
      }

    @annotation.tailrec
    final def step: Future[A] = this match {
      case More(thunk) => thunk().step
      case BindMore(thunk, f) => futureMonad.flatMap(thunk())(f).step
      case _ => this
    }

    def runAsync(onFinish: A => Unit): Unit =
      listen(a => Done(onFinish(a)))

    def run: A = {
      val latch = new java.util.concurrent.CountDownLatch(1)
      @volatile var result: Option[A] = None
      runAsync({ a => result = Some(a); latch.countDown })
      latch.await
      result.get
    }
  }

  object Future {
    case class Now[+A](a: A) extends Future[A]
    case class More[+A](thunk: () => Future[A]) extends Future[A]
    // XXX why not +B?
    case class BindMore[A, +B](thunk: () => Future[A], f: A => Future[B]) extends Future[B]
    case class Async[+A](onFinish: (A => Trampoline[Unit]) => Unit) extends Future[A]
    // XXX why not +B?
    case class BindAsync[A, +B](onFinish: (A => Trampoline[Unit]) => Unit, f: A => Future[B]) extends Future[B]

    val pool = Executors.newCachedThreadPool();

    def submit[A](a: => A): Future[A] = {
      println("Creating async ")
      Async { cb =>
        println("Submitting job to " + pool)
        pool.submit(new Callable[Unit] {
          // XXX book says cb(a).run
          def call = {
            println("Submitted")
            val fut = cb(a)
            tramp.run(fut)
          }
        })
      }
    }

    val fmCount = new AtomicInteger(0)

    // ex10
    def futureMonad = new Monad[Future] {
      def unit[A](a: => A) = Now[A](a)
      def flatMap[A, B](ma: Future[A])(f: A => Future[B]) = {
        try {
          ma match {
            case Now(a) =>
              println("futureMonad.flatMap.now " + fmCount.incrementAndGet())
              f(a)
            case More(thunk) =>
              println("futureMonad.flatMap.more " + fmCount.incrementAndGet())
              BindMore(thunk, f)
            case BindMore(thunk, f2) => {
              println("futureMonad.flatMap.bindmore " + fmCount.incrementAndGet())
              val f3 = f2 andThen { ma => flatMap(ma) { a => f(a) } }
              BindMore(thunk, f3)
            }
            case Async(onFinish) =>
              println("futureMonad.flatMap.async " + fmCount.incrementAndGet())
              BindAsync(onFinish, f)
            case BindAsync(onFinish, f2) => {
              println("futureMonad.flatMap.bindasync " + fmCount.incrementAndGet())
              val f3 = f2 andThen { ma => flatMap(ma) { a => f(a) } }
              BindAsync(onFinish, f3)
            }
          }
        } finally {
          println("futureMonad.flatMap done " + fmCount.getAndDecrement())
        }
      }
    }
  }

  trait Trans[F[_], G[_]] {
    def apply[A](fa: F[A]): G[A]
  }

  //ex11

  val runTcount = new AtomicInteger(0)
  import xxx._
  def runT[F[_], G[_], A](T: Trans[F, G])(G: Monad[G])(io: IO[F, A]): G[A] = {
    try {
      io match {
        case Pure(a) =>
          println("runT.pure " + runTcount.incrementAndGet())
          G.unit(a)
        case Request(expr, k) =>
          println("runT.request " + runTcount.incrementAndGet())
          G.flatMap(T(expr))(e => runT(T)(G)(k(e)))
        case BindRequest(expr, k, f) =>
          println("runT.bindrequest " + runTcount.incrementAndGet())
          G.flatMap(T(expr))(e => runT(T)(G)(k(e) flatMap f))
        case BindMore(k, f) =>
          println("runT.bindmore " + runTcount.incrementAndGet())
          runT(T)(G)(k() flatMap f)
        case xxx.More(k) =>
          println("runT.more " + runTcount.incrementAndGet())
          runT(T)(G)(k())
      }
    } finally {
      println("runT done " + runTcount.getAndDecrement())
    }
  }

  import ch13_4.Console
  import ch13_4.Return
  import ch13_4.ReadLine
  import ch13_4.PrintLine

  object RunConsoleSync extends Trans[Console, Future] {
    def apply[A](c: Console[A]): Future[A] = {
      c match {
        case Return(a) => Future.Now(a)
        case ReadLine => Future.futureMonad.unit({
          try {
            Some(readLine)
          } catch {
            case _: Throwable => None
          }
        })
        case PrintLine(m) => Future.futureMonad.unit({
          println(m)
        })
      }
    }
  }

  object RunConsoleAsync extends Trans[Console, Future] {
    def apply[A](c: Console[A]): Future[A] = {
      c match {
        case Return(a) => Future.Now(a)
        case ReadLine =>
          println("Submitting Readline")
          Future.submit {
            try {
              println("Executing Readline")
              Some(readLine)
            } catch {
              case _: Throwable => None
            }
          }
        case PrintLine(m) => Future.futureMonad.unit({
          println(m)
        })
      }
    }
  }

  if (true) {
    val testFuture: IO[Future, Int] = Request[Future, Int, Int](Future.Now(3), d => ioMonad.unit(d))
    println(xxx.run(Future.futureMonad)(testFuture))
  }
  // println(runT[Console,Future,Unit](RunConsoleSync)(Future.futureMonad)(ch13_5.xxx.program0))
//  val future = runT[Console, Future, Option[String]](RunConsoleAsync)(Future.futureMonad)(ch13_5.xxx.program0)
  val future = runT[Console, Future, Unit](RunConsoleAsync)(Future.futureMonad)(ch13_5.xxx.program2)
  println("Future: " + future)
  println("Future.Result: " + future.run)
  Future.pool.shutdown()
}