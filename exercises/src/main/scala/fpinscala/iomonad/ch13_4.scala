package fpinscala.iomonad

import scala.annotation.tailrec
import java.util.concurrent.atomic.AtomicInteger

object ch13_4 extends App {
  // External is type parameter; but the type parameter was added besides Request to Pure and IO, why????
  // xxx why is External now called Runnable in the book?
  trait External[A] { def run: A }
  object Delay { def apply[A](a: => A) = new External[A] { def run = { println("force"); a } } }

  trait IO[F[_], +A] {
    def map[B](f: A => B) = ioMonad.map(this)(f)
    def flatMap[B](f: A => IO[F, B]) = ioMonad.flatMap(this)(f)
  }

  case class Pure[F[_], +A](a: A) extends IO[F, A]
  // XXX why can't we just use 'receive: I => A'?
  case class Request[F[_], I, A](expr: F[I], receive: I => IO[F, A]) extends IO[F, A]
  //case class Request[E[_],A,B](expr: E[A])(f: A => B) extends IO[A]

  trait Run[F[_]] {
    // XXX use 'I' instead of 'A' to be consistent with parameter names in Request
    // XXX apply returns a pair of I and new state for Run -> this is in advance to be using a monadic Run!!!!!
    def apply[I](request: F[I]): (I, Run[F])
    def unit[I](i: I): F[I] = sys.error("")
    //def apply[I](request: F[I]): I
  }

  object IO {
    @tailrec
    def run[F[_], A](requestRunner: Run[F])(io: IO[F, A]): A = {
      io match {
        case Pure(a) => a
        case Request(expr, receive) =>
          requestRunner.apply(expr) match {
            case (i, requestRunner2) => run(requestRunner2)(receive(i))
          }
      }
    }

    def apply[A](a: => A): IO[External, A] =
      Request(Delay(a), (a: A) => Pure(a))
  }

  // requests to console
  sealed trait Console[A]
  case class Return[A](a: A) extends Console[A]
  case object ReadLine extends Console[Option[String]]
  case class PrintLine(m: String) extends Console[Unit]

  object RunConsoleMock extends Run[Console] {
    override def unit[I](i: I) = Return(i)
    def apply[I](request: Console[I]): (I, Run[Console]) = request match {
      // XXX this is interesting: based on the match, we determine the actual value of I
      case ReadLine => (Some("dummy"), RunConsoleMock)
      case PrintLine(m) => ((), RunConsoleMock)
      case Return(a) => (a, RunConsoleMock)
    }
  }

  object RunConsole extends Run[Console] {
    override def unit[I](i: I) = Return(i)
    def apply[I](request: Console[I]): (I, Run[Console]) = request match {
      case ReadLine => ({ try { Some(readLine) } catch { case _: Throwable => None } }, RunConsole)
      case PrintLine(m) => ({ println(m) }, RunConsole)
      case Return(a) => (a, RunConsole)
    }
  }

  //ex1
  // XXX ok, why did I make the requestRunnera parameter of ioMonad???
  // actual running of external requests is done elsewhere; here, we only compose the runnable program description
  //def ioMonad[F[_]](run: Run[F]) = new Monad[({ type IOF[A] = IO[F,A] })#IOF] {
  def ioMonad[F[_]] = new Monad[({ type IOF[A] = IO[F, A] })#IOF] {
    def unit[A](a: => A) = Pure[F, A](a)
    def flatMap[A, B](ma: IO[F, A])(f: A => IO[F, B]): IO[F, B] = ma match {
      case Pure(a) => f(a)
      case Request(expr, receive) => {
        // because we don't have access to an requestRunner here, we can only fuse the receive and 'f'
        val fused = receive andThen (ma2 => flatMap(ma2)(f))
        Request(expr, fused)
      }
    }
  }

  def console(inputs: List[String]): Run[Console] = new Run[Console] {
    override def unit[I](i: I) = Return(i)
    def apply[I](request: Console[I]): (I, Run[Console]) = request match {
      case ReadLine => inputs match {
        case hd :: tl => (Some(hd), console(tl))
        case Nil => (None, this)
      }
      case PrintLine(m) => ({ println(m) }, this)
      case Return(a) => (a, this)
    }
  }

  // ex3
  // XXX just changed the result from 'A' to 'F[A]'!!!! This is important,
  // because the result is now a value in the underlying monad F
  def runM[F[_], A](requestRunner: Monad[F])(io: IO[F, A]): F[A] = {
    io match {
      case Pure(a) => requestRunner.unit(a)
      case Request(expr, receive) => {
        try {
          println("runM down")
          requestRunner.flatMap(expr)(e => runM(requestRunner)(receive(e)))
        } finally {
          println("runM up")
        }
      }
    }
  }

  def runnerMonad[F[_]](requestRunner: Run[F]) = new Monad[F] {
    // shit: we must somehow use the next state ma1
	val runnerCounter = new AtomicInteger(0)
    var runner = requestRunner
    // XXX this unit() is our invention!!
    def unit[A](a: => A): F[A] = runner.unit(a)
    def flatMap[A, B](ma: F[A])(f: A => F[B]): F[B] = {
      // was passiert mit ma1??
      // is the Runner considered stateless? no -> apply results in new state of Run
      // should the requestRunner variable be updated with ma1 for use during the next flatMap?
      //        requestRunner.apply(ma) match { case (a, ma1) => f(a) }
      try {
        println("runnerMonad.flatMap " + runnerCounter.incrementAndGet())
        runner.apply(ma) match { case (a, ma1) => runner = ma1; f(a) }
      } finally {
        println("runnerMonad.flatMap done" + runnerCounter.getAndDecrement())
      }

    }
  }

  import ch13_3.fahrenheitToCelsius
  def converter: IO[Console, Unit] = {
    for (
      _ <- Request[Console, Unit, Unit](PrintLine("Enter a temperature in degrees fahrenheit: "), { x => ioMonad.unit(x) });
      tfo <- Request[Console, Option[String], Option[Double]](ReadLine, d => ioMonad.unit(d.map { _.toDouble }));
      x <- Request[Console, Int, Int](Return(4711), { x => ioMonad.unit(x) });
      func <- Pure[Console, Option[Double] => Option[Double]](_.map { fahrenheitToCelsius _ });
      res = func(tfo);
      _ <- Request[Console, Unit, Unit](PrintLine("Temperature in degrees celsius: " + res + " => " + x), { x => ioMonad.unit(x) })
    ) yield {}
  }

  //println(better3.IO.run(better3.RunConsole)(better3.converter))
  //println(runM(runnerMonad(RunConsole))(converter))

  println(runM(runnerMonad(console(List("A", "B"))))(
    ioMonad[Console].sequence_(Stream.fill(100)(
      for (
        x <- Request[Console, Option[String], Option[String]](ReadLine, x => ioMonad.unit(x));
        _ <- Request[Console, Unit, Unit](PrintLine("Go: " + x), { x => ioMonad.unit(x) })
      ) yield {}))))
}