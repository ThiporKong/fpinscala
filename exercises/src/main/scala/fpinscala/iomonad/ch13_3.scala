package fpinscala.iomonad

import scala.annotation.tailrec

object ch13_3 extends App {
  def fahrenheitToCelsius(f: Double) = (f - 32) * 5 / 9
  
  object stage1 {
    trait IO {
      def run: Unit
      def ++(next: IO) = new IO { // XXX 'next' is named 'io' in book
        def run = { this.run; next.run }
      }
    }

    object IO {
      def empty = new IO {
        def run = ()
      }
    }
  }

  object stage2 {
    trait IO[+A] { self =>
      def run: A
      def map[B](f: A => B): IO[B] = new IO[B] { def run = f(self.run) }

      // XXX why do we this indirection here???
      def flatMap[B](f: A => IO[B]): IO[B] = new IO[B] { def run = f(self.run).run }
      //def flatMap[B](f: A => IO[B]): IO[B] = f(run)
    }

    // ... and IO now forms a Monad
    object IO extends Monad[IO] {
      // XXX we can use '=>' because a is defined to be pure, so referential transparency is given
      def unit[A](a: => A) = new IO[A] { def run = a }
      override def map[A, B](ma: IO[A])(f: A => B): IO[B] = ma.map(f)
      def flatMap[A, B](ma: IO[A])(f: A => IO[B]): IO[B] = ma.flatMap(f)

      // XXX if we use a call-by-name value as call-by-name parameter, is this value evaluated or not?
      def apply[A](a: => A): IO[A] = unit(a)

      object test_named {
	    // value is only evaluated at 'val y = x'
        def g(x: => Int) { println("g"); val y = x; println("g2", y) }
        def f(x: => Int) { println("f"); g(x) }

        f({ println("NOW"); 3 })
      }
    }

    def ReadLine: IO[String] = IO { readLine }
    def PrintLine(m: String): IO[Unit] = IO { println(m) }

    def converter: IO[Unit] = {
      for (
        _ <- PrintLine("Enter a temperature in degrees fahrenheit: ");
        tf <- ReadLine.map { _.toDouble };
        _ <- PrintLine("Temperature in degrees celsius: " + fahrenheitToCelsius(tf))
      ) yield {}
    }

    //println(better.IO.replicateM(2)(better.converter).run)
  }

  // simple External
  object stage3 {
    // xxx why is External now called Runnable in the book?
    trait External[A] { def run: A }
    object Delay { def apply[A](a: => A) = new External[A] { def run = a } }

    trait IO[+A]
    case class Pure[+A](a: A) extends IO[A]
    // XXX why can't we just use 'receive: I => A'?
    case class Request[I, A](expr: External[I])(receive: I => IO[A]) extends IO[A]
    //case class Request[E[_],A,B](expr: E[A])(f: A => B) extends IO[A]
  }
}