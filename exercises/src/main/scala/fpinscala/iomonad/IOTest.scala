package fpinscala.iomonad

import IO._
import IO.ex4._

object IOTest extends App {
  val io = IO.ex4.IO
  println("hello")
      val F = ioMonad[Runnable]; import F._
    val ex1 = sequence_(Stream.fill(100000)(io { math.random }))
    val ex2 = foreachM(Stream.range(1, 100000))(i => io { println(i) })
    val ex3 = forever(io { println("hi") })
    // foldLeft baut nur eine endlose Kette von Requests auf
    val ex4 = Stream.fill(6)("hi").foldLeft(io { () })(
      (acc, s) => acc *> io { println(s) })
      
      object RunnableRunner extends Run[Runnable] {
            def apply[A](expr: Runnable[A]): (A, Run[Runnable]) = (expr.run, this)
      }
      
    //ex4
    List(/*ex1, ex2, ex3,*/ ex4).foreach { ex =>
      IO.initialRun.run(RunnableRunner)(ex)
      }
}