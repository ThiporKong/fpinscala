package fpinscala.iomonad

import scala.annotation.tailrec

object IO {
  //ex1
  trait IO[F[_], +A]
  case class Pure[F[_], +A](get: A) extends IO[F, A]
  case class Request[F[_], I, +A](
    depth: Int,
    expr: F[I],
    receive: I => IO[F, A]) extends IO[F, A]

  sealed trait Console[A]
  case object ReadLine extends Console[Option[String]]
  case class PrintLine(s: String) extends Console[Unit]

  trait Run[F[_]] {
    def apply[A](expr: F[A]): (A, Run[F])
  }

  //ex2
  object RunConsole extends Run[Console] {
    // how does Scala match, that the A in the apply[A] is actually a concrete Option[String] for ReadLine etc?
    def apply[A](expr: Console[A]): (A, Run[Console]) = {
      expr match {
        case ReadLine => (Some(readLine), RunConsole)
        case PrintLine(s) =>
          println(s)
          ((), RunConsole)
      }
    }
  }

  //ex2
  case class DummyRunConsole(lines: List[String]) extends Run[Console] {
    def apply[A](expr: Console[A]): (A, Run[Console]) = {
      expr match {
        case ReadLine =>
          lines match {
            case hd :: tl => (Some(hd), DummyRunConsole(tl))
            case Nil => (None, this)
          }
        case PrintLine(s) =>
          println(s)
          //((), RunConsole) -> WRONG, must not forget our input 'lines'
          ((), this)
      }
    }
  }
  val console = DummyRunConsole(_)

  def monad[F[_]] = new Monad[({ type f[a] = IO[F, a] })#f] {
    def unit[A](a: => A): IO[F, A] = Pure(a)
    def flatMap[A, B](a: IO[F, A])(f: A => IO[F, B]): IO[F, B] =
      a match {
        case Pure(av) => f(av)
        case Request(d, expr, recv) =>
          val recv2 = recv andThen (flatMap(_)(f))
          Request(d+1,expr, recv2)
    }
  }

  def ioMonad[F[_] /*: Monad*/ ] = new Monad[({ type IOF[A] = IO[F, A] })#IOF] {
    //val fm = implicitly[Monad[F]]
    def unit[A](a: => A): IO[F, A] = Pure[F, A](a)
    def flatMap[A, B](a: IO[F, A])(f: A => IO[F, B]): IO[F, B] =
      a match {
        case Pure(av) => f(av)
        //case Request(expr,recv) => Request(expr, recv andThen (_ flatMap f))
        //case Request(expr,recv) => Request(expr, recv andThen (flatMap(_)(f) ))
        case Request(d, expr, recv) =>
          // XXX can't work with explicit parameters here, must use andThen
          println("Request", d, expr, recv)
          val recv2 = recv andThen (flatMap(_)(f))
          Request(d+1, expr, recv2)
      }
  }

  object initialRun {
    def run[F[_], A](R: Run[F])(io: IO[F, A]): A = io match {
      case Pure(a) => a
      case Request(_, expr, recv) => R(expr) match {
        case (e, r2) => run(r2)(recv(e))
      }
    }

    run(RunConsole)(ioMonad[Console].unit(1))
  }
  // ex3
  // XXX reference is to "Run" that they mean 'run' function; the original run returns an A!!
  // but Console is no monad???
  // the part is missing, there a Monad implementation for RunConsole is to be provided
  object ex3 {
    def run[F[_]: Monad, A](io: IO[F, A]): F[A] = {
      val F = implicitly[Monad[F]]
      io match {
        case Pure(a) => F.unit(a)
        case Request(_, expr, recv) => F.flatMap(expr)(e => run(recv(e)))
      }
    }

  }
  
  object ex4 {
    object IO {
      @annotation.tailrec
      def run[F[_], A](R: Run[F])(io: IO[F, A]): A = io match {
        case Pure(a) => a
        case Request(_, expr, recv) =>
          R(expr) match { case (e, r2) => run(r2)(recv(e)) }
      }

      def apply[A](a: => A): IO[Runnable, A] =
        Request(0, Delay(a), (a: A) => Pure(a))
    }
    trait Runnable[A] { def run: A }
    object Delay { def apply[A](a: => A) = new Runnable[A] { def run = a } }

    val F = ioMonad[Runnable]; import F._
    val ex1 = sequence_(Stream.fill(100000)(IO { math.random }))
    val ex2 = foreachM(Stream.range(1, 100000))(i => IO { println(i) })
    val ex3 = forever(IO { println("hi") })
    val ex4 = Stream.fill(100000)("hi").foldLeft(IO { () })(
      (acc, s) => acc *> IO { println(s) })
  }
  
  object ex5 {
    trait Trampoline[+A] { def run: A = Trampoline.run(this) }
	  case class Done[+A](get: A) extends Trampoline[A]
	  case class More[+A](force: () => Trampoline[A]) extends Trampoline[A]
	  case class Bind[A,+B](force: () => Trampoline[A], f: A => Trampoline[B]) extends Trampoline[B]

// companion object itself is the Monad instance
	object Trampoline extends Monad[Trampoline] {
	  //def map[A,B](a: F[A])(f: A => B): F[B] = flatMap(a)(a => unit(f(a)))
	    //ex6
        def unit[A](a: => A): Trampoline[A] = Done(a)
	    def flatMap[A, B](a: Trampoline[A])(f: A => Trampoline[B]): Trampoline[B] =
	      a match {
          case Done(a) => f(a)
          case More(force) => Bind(force, f) // just return
          // well
          case Bind(force,g) => More(() => Bind(force, g andThen (_ flatMap f)))
          }

	    //ex5
	    @tailrec
		def run[A](t: Trampoline[A]): A = 
		  t match {
		  case Done(a) => a
		  case More(f) => run(f())
		  //case Bind(force, f) => run(f(force())) // wieso funktioniert f(force())???
          case Bind(force, f) => run(force() flatMap f) // the force() is the magic!
		  }
	}
  }
}