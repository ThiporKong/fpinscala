package fpinscala.iomonad

import scala.annotation.tailrec
import java.util.concurrent.atomic.AtomicInteger

object ch13_5 extends App {
  import ch13_4._

  object ex4 {
    val F = ioMonad[External]
    import F._

    //ex4
    val ex0 = replicateM(1700)(IO { math.random })
    val ex1 = sequence_(Stream.fill(10)(IO { math.random }))
    val ex2 = foreachM(Stream.range(1, 10))(i => IO { println(i) })
    val ex3 = forever(IO { println("hi") })
    val ex4 = Stream.fill(10)("hi").foldLeft(IO { () }) {
      (acc, s) => ioMonad.map2(acc, IO { println(s) })((_, b) => b)
    }

    case object ExternalRunner extends Run[External] {
      def apply[I](request: External[I]): (I, Run[External]) = (request.run, this)
      override def unit[I](i: I): External[I] = new External[I] { def run = i }
    }

    // one problem is that runM is not tail-recursive, filling up the stack
    def test = List(ex0, ex1, ex2, ex4, ex3).foreach { ex =>
      println(runM(runnerMonad(ExternalRunner))(ex).run)
    }
  }

  object tramp {
    sealed trait Trampoline[+A]
    case class Done[+A](get: A) extends Trampoline[A]
    case class More[+A](force: () => Trampoline[A]) extends Trampoline[A]
    case class Bind[A, +B](force: () => Trampoline[A], f: A => Trampoline[B]) extends Trampoline[B]

    // ex5 -- XXX can't be solved without having flatMap -> having solved ex6 before????
    @tailrec
    def run[A](t: Trampoline[A]): A = {
      t match {
        case Done(get) => get
        case More(force) => run(force())
        // why does this compile????
        //case Bind(force, f) => { run(f(force())) }// the only place for actual function applications
        case Bind(force, f) => {
          if (true) {
            def flatMapIntern[A, B](ma: Trampoline[A])(f: A => Trampoline[B]): Trampoline[B] = ma match {
              case Done(get) => f(get)
              case More(force) => Bind(force, f)
              case Bind(force, f2) => Bind(force, f2.andThen { flatMapIntern(_)(f) })
            }
            run(flatMapIntern(force())(f))
          } else {
            run(monadTrampoline.flatMap(force())(f))
          }
        }
      }
    }

    // ex6
    def monadTrampoline = new Monad[Trampoline] {
      def unit[A](a: => A): Trampoline[A] = Done(a)
      def flatMap[A, B](ma: Trampoline[A])(f: A => Trampoline[B]) = ma match {
        case Done(get) => f(get)
        case More(force) => Bind(force, f)
        case Bind(force, f2) => Bind(force, f2.andThen { flatMap(_)(f) })
      }
    }
  }

  object xxx {
    sealed trait IO[F[_], +A] {
      def map[B](f: A => B) = ioMonad.map(this)(f)
      def flatMap[B](f: A => IO[F, B]) = ioMonad.flatMap(this)(f)
    }

    case class Pure[F[_], +A](get: A) extends IO[F, A] // aaaahhh. here we are!

    case class Request[F[_], I, +B](expr: F[I], receive: I => IO[F, B]) extends IO[F, B]
    case class BindRequest[F[_], I, A, +B](expr: F[I], receive: I => IO[F, A], f: A => IO[F, B]) extends IO[F, B]
    case class More[F[_], +B](force: () => IO[F, B]) extends IO[F, B]
    case class BindMore[F[_], A, +B](force: () => IO[F, A], f: A => IO[F, B]) extends IO[F, B]

    //ex8
    @tailrec
    def run[F[_], A](R: Run[F])(io: IO[F, A]): A = {
      def ioflatMap[X] = ioMonad[F].flatMap[X, A] _
      io match {
        case Pure(get) => get
        case Request(expr, receive) => {
          val (i, r2) = R(expr)
          run(r2)(receive(i))
        }
        case BindRequest(expr, receive, f) => {
          val (i, r2) = R(expr)
          run(r2)(ioflatMap(receive(i))(f))
        }
        case More(force) => run(R)(force())
        case BindMore(force, f) => run(R)(f(force()))
      }
    }

    val runCounter = new AtomicInteger(0)
    //ex8
    //@tailrec // XXX NOT TAIL-RECURSIVE
    def run[F[_], A](F: Monad[F])(io: IO[F, A]): F[A] = {
      println("run: " + runCounter.incrementAndGet())
      try {
        def ioflatMap[X] = ioMonad[F].flatMap[X, A] _
        io match {
          case Pure(get) => F.unit(get)
          case Request(expr, receive) =>
            F.flatMap(expr)(e => run(F)(receive(e)))
          case BindRequest(expr, receive, f) => {
            F.flatMap(expr)(e => run(F)(ioflatMap(receive(e))(f)))
          }
          case More(force) => run(F)(force())
          case BindMore(force, f) => run(F)(ioflatMap(force())(f))
        }
      } finally {
        println("run done: " + runCounter.getAndDecrement())
      }
    }

    val ioCounter = new AtomicInteger(0)

    //ex9
    def ioMonad[F[_]]: Monad[({ type f[x] = IO[F, x] })#f] = new Monad[({ type f[x] = IO[F, x] })#f] {
      def unit[A](a: => A) = Pure(a)
      def flatMap[A, B](ma: IO[F, A])(f: A => IO[F, B]) = {
        println("ioMonad.flatMap " + ioCounter.incrementAndGet())
        try {
          ma match {
            case Pure(get) => f(get)
            case Request(expr, receive) => BindRequest(expr, receive, f)
            case BindRequest(expr, receive, g) => BindRequest(expr, receive, g andThen { _ flatMap f })
            case More(force) => BindMore(force, f)
            case BindMore(force, g) => BindMore(force, g andThen { _ flatMap f })
          }
        } finally {
          println("ioMonad.flatMap done " + ioCounter.getAndDecrement())
        }
      }
    }

    val program0 = for (
      _ <- Request[Console, Unit, Unit](PrintLine("Enter: "), { x => ioMonad.unit(x) });
      x <- Request[Console, Option[String], Option[String]](ReadLine, x => ioMonad.unit(x));
      _ <- Request[Console, Unit, Unit](PrintLine("Got: " + x), { x => ioMonad.unit(x) })
    ) yield { x }

    val program = ioMonad[Console].sequence_(Stream.fill(100000)(program0))

    val program2 = ioMonad[Console].replicateM_(50000)(Request[Console, Int, Int](Return(3), x => ioMonad.unit(x)))

    //def test = println(runM(runnerMonad(console(List("A", "B"))))(program))
    def test = run(console(List("A", "B")))(program2)
    def test2 = run(runnerMonad(console(List("A", "B"))))(program2)
  }

  //println(xxx.test2)
  println(ex4.test)
}