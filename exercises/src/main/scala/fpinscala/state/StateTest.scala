package fpinscala.state

object StateTest extends App {
  val rng = RNG.simple(4711)
  println(RNG.ints(5)(rng))
  println(RNG.double(rng))
  println(RNG.sequence(List(
      RNG.ints(5),
      RNG.double
      ))(rng))
}