package fpinscala.state

trait RNG {
  def nextInt: (Int, RNG) // Should generate a random `Int`. We will later define other functions in terms of `nextInt`.
}

object RNG {
  def simple(seed: Long): RNG = new RNG {
    def nextInt = {
      val seed2 = (seed * 0x5DEECE66DL + 0xBL) & // `&` is bitwise AND
        ((1L << 48) - 1) // `<<` is left binary shift
      ((seed2 >>> 16).asInstanceOf[Int], // `>>>` is right binary shift with zero fill
        simple(seed2))
    }
  }

  type Rand[+A] = RNG => (A, RNG)

  def unit[A](a: A): Rand[A] =
    rng => (a, rng)

  def map[A, B](s: Rand[A])(f: A => B): Rand[B] =
    rng => {
      val (a, rng2) = s(rng)
      (f(a), rng2)
    }

  def map2[A, B, C](ra: Rand[A], rb: Rand[B])(f: (A, B) => C): Rand[C] = ex7.map2(ra, rb)(f)
  def flatMap[A, B](s: Rand[A])(f: A => Rand[B]): Rand[B] = ex9.flatMap(s)(f)
  def sequence[A](fs: List[Rand[A]]): Rand[List[A]] = ex8.sequence(fs)

  val int: Rand[Int] = _.nextInt

  //ex1
  def positiveInt: Rand[Int] = rng => {
    val (res, rng2) = rng.nextInt
    if (res != Int.MinValue) (res.abs, rng2) else positiveInt(rng2)
  }

  //ex2
  def double: Rand[Double] = rng => {
    val (i, rng2) = positiveInt(rng: RNG)
    (i.toDouble / Int.MaxValue.toDouble, rng2)
  }

  //ex3
  def intDouble(rng: RNG): ((Int, Double), RNG) = {
    val (i, rng2) = positiveInt(rng)
    val (d, rng3) = double(rng2)
    ((i, d), rng3)
  }

  def doubleInt(rng: RNG): ((Double, Int), RNG) = {
    val (i1, rng2) = positiveInt(rng)
    val (i2, rng3) = positiveInt(rng2)
    ((i1, i2), rng3)
  }

  def double3(rng: RNG): ((Double, Double, Double), RNG) = {
    val (d1, rng2) = double(rng)
    val (d2, rng3) = double(rng2)
    val (d3, rng4) = double(rng3)
    ((d1, d2, d3), rng4)
  }

  //ex4
  def ints(count: Int): Rand[List[Int]] =
    sequence((1 to count).map { _ => RNG.int }.toList)

  //ex5
  def positiveMax(n: Int): Rand[Int] = map(double) { d => (d * n.toDouble).toInt }

  // ex6

  // ex7
  object ex7 {
    def map2[A, B, C](ra: Rand[A], rb: Rand[B])(f: (A, B) => C): Rand[C] =
      flatMap(ra) { a => map(rb) { b => f(a, b) } }
  }

  // ex8
  object ex8 {
    def sequence[A](fs: List[Rand[A]]): Rand[List[A]] =
      map(fs.foldLeft(unit(Nil: List[A])) { (acc, x) => map2(acc, x) { (acc, x) => x :: acc } }) { l => l.reverse }
  }

  // ex9
  object ex9 {
    def flatMap[A, B](s: Rand[A])(f: A => Rand[B]): Rand[B] = rng => {
      val (a, rng2) = s(rng)
      f(a)(rng2)
    }
  }

  object ex10 {
    def map[A, B](s: Rand[A])(f: A => B): Rand[B] =
      flatMap(s) { a => unit(f(a)) }
    def map2[A, B, C](ra: Rand[A], rb: Rand[B])(f: (A, B) => C): Rand[C] =
      flatMap(ra) { a => map(rb) { b => f(a, b) } }
  }
}

case class State[S, +A](run: S => (A, S)) {
  def flatMap[B](f: A => State[S, B]): State[S, B] = {
    val run2 = { s: S =>
      val (a, s2) = run(s)
      f(a).run(s2)
    }
    State(run2)
  }

  def map[B](f: A => B): State[S, B] = flatMap { a => State.unit(f(a)) }

  def map2[B, C](sb: State[S, B])(f: (A, B) => C): State[S, C] =
    flatMap { a => sb.map { b => f(a, b) } }
}

import State._
sealed trait Input {
  def action: State[Machine, Unit]
}
case object Coin extends Input {
  override def action =
    get[Machine].flatMap { m =>
      set(m.copy(locked = m.candies > 0, coins = m.coins + 1))
    }
}

case object Turn extends Input {
  override def action =
    get[Machine].flatMap { m =>
      set(m.copy(locked = true, candies = m.candies - 1))
    }
}

case class Machine(locked: Boolean, candies: Int, coins: Int)

object State {
  def unit[S, A](a: A) = State({ s: S => (a, s) })
  def map2[S, A, B, C](ra: State[S, A], rb: State[S, B])(f: (A, B) => C): State[S, C] =
    ra.flatMap { a => rb.map { b => f(a, b) } }

  def get[S]: State[S, S] = State({ s => (s, s) })
  def set[S](newState: S): State[S, Unit] = State({ _ => ((), newState) })

  def sequence[S, A](fs: List[State[S, A]]): State[S, List[A]] =
    fs.foldRight(unit[S, List[A]](Nil: List[A])) { (x, acc) => map2(acc, x) { (acc, x) => x :: acc } }

  //ex12
  def modify[S](f: S => S): State[S, Unit] = for {
    s <- get
    _ <- set(f(s))
  } yield ()

  type Rand[A] = State[RNG, A]

  // ex13
  def getCoins = get[Machine].map { _.coins }

  def simulateMachine(inputs: List[Input]): State[Machine, Int] =
    State.sequence(inputs.map { _.action }).flatMap(_ => getCoins)

  val (coin, machine) = simulateMachine(List(Coin, Turn, Coin, Turn)).run(Machine(false, 10, 10))
  println(coin)
}