package fpinscala
package monads

import parsing._
import testing._
import parallelism._
import state._
import parallelism.Par._

trait Functor[F[_]] {
  def map[A,B](fa: F[A])(f: A => B): F[B]

  def distribute[A,B](fab: F[(A, B)]): (F[A], F[B]) =
    (map(fab)(_._1), map(fab)(_._2))

  // ex6
  def cofactor[A,B](e: Either[F[A], F[B]]): F[Either[A, B]] =
    e match {
    case Left(fa) => map(fa){ a => Left[A,B](a)}
    case Right(fb) => map(fb) { b => Right[A,B](b) }
	}
  
  //ex7
  //ex8
}

object Functor {
  val listFunctor = new Functor[List] {
    def map[A,B](as: List[A])(f: A => B): List[B] = as map f
  }
}

trait Monad[M[_]] extends Functor[M] {
  def unit[A](a: => A): M[A]

  def map[A,B](ma: M[A])(f: A => B): M[B] =
    flatMap(ma)(a => unit(f(a)))
  def map2[A,B,C](ma: M[A], mb: M[B])(f: (A, B) => C): M[C] =
    flatMap(ma)(a => map(mb)(b => f(a, b)))

  //ex3
  def sequence[A](lma: List[M[A]]): M[List[A]] = 
    lma.foldRight(unit(List[A]()))((a, as) => flatMap(a) { a => map(as) { as => a::as } })

  def traverse[A,B](la: List[A])(f: A => M[B]): M[List[B]] =
    sequence(la.map { f })

  //ex4
  def replicateM[A](n: Int, ma: M[A]): M[List[A]] =
    sequence(List.fill(n)(ma))
  
  //ex5
    // List: creates all possible combinations for lists of length n with elements from ma
    // Option: None => None, Some(x) => Some(List.fill(n)(x))

  //ex10/ex16
  def compose[A,B,C](f: A => M[B], g: B => M[C]): A => M[C] =
    a => join(map(f(a)){b => g(b)})
    //a => flatMap(f(a)) { b => g(b) }

  // ex11
  // Implement in terms of `compose`:
  def _flatMap[A,B](ma: M[A])(f: A => M[B]): M[B] =
    compose({_:Unit => ma}, f)(())

    //ex12
    //ex13
    
  //ex14
  def join[A](mma: M[M[A]]): M[A] =
    flatMap(mma)(identity)

  def flatMap[A,B](ma: M[A])(f: A => M[B]): M[B]
    
  //ex17
}

case class Reader[R, A](run: R => A)

object Monad {
  //import fpinscala.testing.ch8._
  
  val genMonad = new Monad[Gen] {
    def unit[A](a: => A): Gen[A] = Gen.unit(a)
    override def flatMap[A,B](ma: Gen[A])(f: A => Gen[B]): Gen[B] =
      ma flatMap f
  }

  // ex1
  import fpinscala.parallelism.ch7.Par._
  
  val parMonad: Monad[Par] = new Monad[Par] {
    override def unit[A](a: => A): Par[A] = Par.unit(a)
    override def flatMap[A,B](ma: Par[A])(f: A => Par[B]): Par[B] = flatMap(ma)(f)
  }

  def parserMonad[P[+_]](p: Parsers[_,P]): Monad[P] = new Monad[P] {
    override def unit[A](a: => A): P[A] = p.unit(a)
    override def flatMap[A,B](ma: P[A])(f: A => P[B]): P[B] = p.flatMap(ma)(f)
  }

  val optionMonad: Monad[Option] = new Monad[Option] {
    override def unit[A](a: => A): Option[A] = Some(a)
    override def flatMap[A,B](ma: Option[A])(f: A => Option[B]): Option[B] = ma.flatMap(f)
  }

  val streamMonad: Monad[Stream] = new Monad[Stream] {
    override def unit[A](a: => A): Stream[A] = Stream(a)
    override def flatMap[A,B](ma: Stream[A])(f: A => Stream[B]): Stream[B] = ma.flatMap(f)
  }

  val listMonad: Monad[List] = new Monad[List] {
    override def unit[A](a: => A): List[A] = List(a)
    override def flatMap[A,B](ma: List[A])(f: A => List[B]): List[B] = ma.flatMap(f)
  }

  //ex2
  import fpinscala.state.State
  def stateMonad[S,A] = new Monad[({type StateS[A] = State[S,A]})#StateS] {
    override def unit[A](a: => A): State[S,A] = State.unit(a)
    override def flatMap[A,B](ma: State[S,A])(f: A => State[S,B]): State[S,B] = ma.flatMap(f)
  }

  // ex19
  val idMonad: Monad[Id] = new Monad[Id] {
    override def unit[A](a: => A): Id[A] = Id(a)
    override def flatMap[A,B](ma: Id[A])(f: A => Id[B]): Id[B] = f(ma.value)
  }

  def readerMonad[R] = Reader.readerMonad[R]
}

case class Id[A](value: A) {
  def map[B](f: A => B): Id[B] = ???
  def flatMap[B](f: A => Id[B]): Id[B] = ???
}

//ex22
object Reader {
  def readerMonad[R] = new Monad[({type f[x] = Reader[R,x]})#f] {
    def unit[A](a: => A): Reader[R,A] = Reader {_ => a}
    override def flatMap[A,B](st: Reader[R,A])(f: A => Reader[R,B]): Reader[R,B] =
      Reader { r => st match { case Reader(arun) => f(arun(r)).run(r) }}
	def get[A](st: Reader[R,A]): Reader[R,A] = flatMap(st)(unit(_))
  }
}