package fpinscala.monads

object MonadTest extends App {
  println(Monad.listMonad.replicateM(5, List(1,2)))
  println(Monad.optionMonad.replicateM(3, Some(3)))
}