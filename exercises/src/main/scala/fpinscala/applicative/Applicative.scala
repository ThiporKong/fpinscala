package fpinscala
package applicative

import monads.Functor
import state._
import State._
import StateUtil._ // defined at bottom of this file
import monoids._

trait Applicative[F[_]] extends Functor[F] {

  //ex1
  def map2[A, B, C](fa: F[A], fb: F[B])(f: (A, B) => C): F[C] =
    apply(apply(unit(f.curried))(fa))(fb)

  //ex1
  def apply[A, B](fab: F[A => B])(fa: F[A]): F[B] =
    map2(fa, fab) { (a, f) => f(a) }

  def unit[A](a: => A): F[A]

  def map[A, B](fa: F[A])(f: A => B): F[B] =
    apply(unit(f))(fa)

  //ex2
  def sequence[A](fas: List[F[A]]): F[List[A]] =
    fas.foldRight(unit(List[A]()))((a, as) => map2(a, as) { _ :: _ })

  //ex2
  def traverse[A, B](as: List[A])(f: A => F[B]): F[List[B]] =
    sequence(as.map { f })

  //ex2
  def replicateM[A](n: Int, fa: F[A]): F[List[A]] =
    sequence(List.fill(n)(fa))

  //ex2
  // XXX fb has wrong type
  //def factor[A,B](fa: F[A], fb: F[A]): F[(A,B)] =
  def factor[A, B](fa: F[A], fb: F[B]): F[(A, B)] =
    map2(fa, fb) { (_ -> _) }

  //ex6 -- XXX this doesn't belong into the trait!!!
  def product[G[_]](G: Applicative[G]): Applicative[({ type f[x] = (F[x], G[x]) })#f] = {
    val self = this
    new Applicative[({ type f[x] = (F[x], G[x]) })#f] {
      def unit[A](a: => A) = (self.unit(a), G.unit(a))
      override def apply[A, B](fab: (F[A => B], G[A => B]))(fa: (F[A], G[A])): (F[B], G[B]) =
        (self.apply(fab._1)(fa._1), G.apply(fab._2)(fa._2))
    }
  }

  //ex7
  //def map2[A, B, C](fa: F[A], fb: F[B])(f: (A, B) => C): F[C] =
  def compose[G[_]](G: Applicative[G]): Applicative[({ type f[x] = F[G[x]] })#f] = {
    val self = this
    new Applicative[({ type f[x] = (F[G[x]]) })#f] {
      def unit[A](a: => A) = (self.unit(G.unit(a)))
      override def apply[A, B](fab: (F[G[A => B]]))(fa: (F[G[A]])): (F[G[B]]) = {
        self.map2(fab, fa) { (gab, ga) =>
          G.map2(gab, ga) { _(_) }
        }
      }
    }
  }

  // ex9
  def sequenceMap[K, V](ofa: Map[K, F[V]]): F[Map[K, V]] =
    ofa.foldRight(unit(Map[K, V]())) {
      case ((k, fv), as) =>
        map2(as, fv) { (as, v) => as + (k -> v) }
    }
}

case class Tree[+A](head: A, tail: List[Tree[A]])

trait Monad[M[_]] extends Applicative[M] {
  def flatMap[A, B](ma: M[A])(f: A => M[B]): M[B] = join(map(ma)(f))

  def join[A](mma: M[M[A]]): M[A] = flatMap(mma)(ma => ma)

  def compose[A, B, C](f: A => M[B], g: B => M[C]): A => M[C] =
    a => flatMap(f(a))(g)

  override def apply[A, B](mf: M[A => B])(ma: M[A]): M[B] =
    flatMap(mf)(f => map(ma)(a => f(a)))
}

object Monad {
  //ex3
  def eitherMonad[E]: Monad[({ type f[x] = Either[E, x] })#f] = new Monad[({ type f[x] = Either[E, x] })#f] {
    def unit[A](a: => A): Either[E, A] = Right[E, A](a)
    override def flatMap[A, B](ma: Either[E, A])(f: A => Either[E, B]): Either[E, B] = {
      ma match {
        case Left(e) => Left[E, B](e)
        case Right(a) => f(a)
      }
    }
  }

  def stateMonad[S] = new Monad[({ type f[x] = State[S, x] })#f] {
    def unit[A](a: => A): State[S, A] = State(s => (a, s))
    override def flatMap[A, B](st: State[S, A])(f: A => State[S, B]): State[S, B] =
      st flatMap f
    // XXX must be overriden!!!!
    override def map[A, B](st: State[S, A])(f: A => B): State[S, B] =
      st map f
  }

  //ex16
  def composeM[M[_], N[_]](implicit M: Monad[M], N: Monad[N], T: Traverse[N]): Monad[({ type f[x] = M[N[x]] })#f] =
    new Monad[({ type f[x] = M[N[x]] })#f] {
      def unit[A](a: => A) = M.unit(N.unit(a))
	  override def join[A](mnmna: M[N[M[N[A]]]]): M[N[A]] =
	    M.flatMap(mnmna) { nmna => M.flatMap(T.sequence(nmna))(nna => M.unit(N.join(nna)) ) }
    }
}

sealed trait Validation[+E, +A]

case class Failure[E](head: E, tail: Vector[E])
  extends Validation[E, Nothing]

case class Success[A](a: A) extends Validation[Nothing, A]

object Applicative {

  //ex4
  def validationApplicative[E]: Applicative[({ type f[x] = Validation[E, x] })#f] = new Applicative[({ type f[x] = Validation[E, x] })#f] {
    def unit[A](a: => A): Validation[E, A] = Success(a)
    override def apply[A, B](fab: Validation[E, A => B])(fa: Validation[E, A]): Validation[E, B] =
      (fa, fab) match {
        case (Failure(head, tail), Failure(headf, tailf)) => Failure(headf, tailf ++ (head +: tail))
        case (f @ Failure(_, _), _) => f
        case (_, f @ Failure(_, _)) => f
        case (Success(a), Success(fa)) => Success(fa(a))
      }
  }

  //ex5
  // Proof the applicative laws follow from monad laws

  type Const[A, B] = A

  implicit def monoidApplicative[M](M: Monoid[M]) =
    new Applicative[({ type f[x] = Const[M, x] })#f] {
      // XXX should better write Const[M,A/B] instead of just M
      //      def unit[A](a: => A): M = M.zero
      //      override def apply[A, B](m1: M)(m2: M): M = M.op(m1, m2)
      def unit[A](a: => A): Const[M, A] = M.zero
      override def apply[A, B](m1: Const[M, A])(m2: Const[M, B]): Const[M, B] = M.op(m1, m2)
    }
}

trait Traverse[F[_]] extends Functor[F] with Foldable[F] {
  def traverse[M[_]: Applicative, A, B](fa: F[A])(f: A => M[B]): M[F[B]] =
    sequence(map(fa)(f))
  def sequence[M[_]: Applicative, A](fma: F[M[A]]): M[F[A]] =
    traverse(fma)(ma => ma)

  type Id[A] = A
  val idMonad = new Monad[Id] {
    def unit[A](a: => A) = a
    override def flatMap[A, B](a: A)(f: A => B): B = f(a)
  }

  // XXX should'nt this come from Functor??
  def map[A, B](fa: F[A])(f: A => B): F[B] = {
    traverse[Id, A, B](fa)(f)(idMonad)
  }

  import Applicative._

  override def foldMap[A, B](as: F[A])(f: A => B)(mb: Monoid[B]): B =
    traverse[({ type f[x] = Const[B, x] })#f, A, Nothing](
      as)(f)(monoidApplicative(mb))

  def traverseS[S, A, B](fa: F[A])(f: A => State[S, B]): State[S, F[B]] =
    traverse[({ type f[x] = State[S, x] })#f, A, B](fa)(f)(Monad.stateMonad)

  def mapAccum[S, A, B](fa: F[A], s: S)(f: (A, S) => (B, S)): (F[B], S) =
    traverseS(fa)((a: A) => (for {
      s1 <- StateUtil.get[S]
      (b, s2) = f(a, s1)
      _ <- StateUtil.set(s2)
    } yield b)).run(s)

  override def toList[A](fa: F[A]): List[A] =
    mapAccum(fa, List[A]())((a, s) => ((), a :: s))._2.reverse

  def zipWithIndex[A](fa: F[A]): F[(A, Int)] =
    mapAccum(fa, 0)((a, s) => ((a, s), s + 1))._1

  //ex12 -- wow. didn't got this. should check if this really works for list
  def reverse[A](fa: F[A]): F[A] =
    mapAccum(fa, toList(fa))((_, as) => (as.head, as.tail))._1

  // ex13
  override def foldLeft[A, B](fa: F[A])(z: B)(f: (B, A) => B): B =
    mapAccum(fa, z) { (a, b) => (b, f(b, a)) }._2
    
  // ex13
  override def foldRight[A, B](fa: F[A])(z: B)(f: (A, B) => B): B =
    mapAccum(reverse(fa), z) { (a, b) => (b, f(a, b)) }._2

  //ex14
  def fuse[M[_], N[_], A, B](fa: F[A])(f: A => M[B], g: A => N[B])(implicit M: Applicative[M], N: Applicative[N]): (M[F[B]], N[F[B]]) =
    traverse[({ type MN[X] = (M[X], N[X]) })#MN, A, B](fa) { a => (f(a), g(a)) }(M.product(N))

    // ex15
  def compose[G[_]](implicit G: Traverse[G]): Traverse[({ type f[x] = F[G[x]] })#f] = {
      val self = this
      new Traverse[({ type f[x] = F[G[x]] })#f] {
		  override def traverse[M[_]: Applicative, A, B](fga: F[G[A]])(f: A => M[B]): M[F[G[B]]] =
		    self.traverse(fga) { ga => G.traverse(ga) { a => f(a) } }
      }
    }
}

/*
  * trait Traverse[F[_]] extends Functor[F] with Foldable[F] {
 *   def traverse[M[_]: Applicative, A, B](fa: F[A])(f: A => M[B]): M[F[B]] =
    sequence(map(fa)(f))
  def sequence[M[_]: Applicative, A](fma: F[M[A]]): M[F[A]] =
    traverse(fma)(ma => ma)
 */
object Traverse {
  //ex10
  val listTraverse = new Traverse[List] {
    //override def foldRight[A, B](as: List[A])(z: B)(f: (A, B) => B): B = ListFoldable.foldRight(as)(z)(f)

    override def traverse[M[_]: Applicative, A, B](fa: List[A])(f: A => M[B]): M[List[B]] = {
      val app = implicitly[Applicative[M]]
      fa.map(f).foldRight(app.unit(List[B]()))((mb, mbs) => app.map2(mb, mbs)(_ :: _))
    }
  }

  //ex10
  val optionTraverse = new Traverse[Option] {
    //override def foldRight[A, B](as: Option[A])(z: B)(f: (A, B) => B): B = OptionFoldable.foldRight(as)(z)(f)

    override def traverse[M[_]: Applicative, A, B](fa: Option[A])(f: A => M[B]): M[Option[B]] = {
      val app = implicitly[Applicative[M]]
      // what the actual rule here, when bs and b are present?
      fa.map(f).foldRight(app.unit(None: Option[B]))((mb, mbs) => app.map2(mb, mbs)((b, bs) => bs.orElse(Some(b))))
    }
  }

  //ex10
  val treeTraverse = new Traverse[Tree] {
    //def foldRight[A, B](as: Tree[A])(z: B)(f: (A, B) => B): B = sys.error("not needed")

    override def sequence[M[_]: Applicative, A](fma: Tree[M[A]]): M[Tree[A]] = {
      val app = implicitly[Applicative[M]]
      val head = fma.head
      val tail = fma.tail.map { sequence(_) }
      val tail2 = listTraverse.sequence(tail)
      app.map2(head, tail2) { (h, t) => Tree(h, t) }
    }
  }

  //ex8
  object ex8 {
    def compose[M[_], N[_]](M: Monad[M])(N: Monad[N]): Monad[({ type MN[A] = M[N[A]] })#MN] = new Monad[({ type MN[A] = M[N[A]] })#MN] {
      override def unit[A](a: => A) = M.unit(N.unit(a))
      override def flatMap[A, B](mna: M[N[A]])(f: A => M[N[B]]): M[N[B]] = {
        //        M.flatMap(mna) { na =>
        //          N.flatMap(na) { a =>
        //            f(a)
        //          }
        //        }
        sys.error("impossible in general")
      }
    }
  }
}

// The `get` and `set` functions on `State` are used above,
// but aren't in the `exercises` subproject, so we include
// them here
object StateUtil {

  def get[S]: State[S, S] =
    State(s => (s, s))

  def set[S](s: S): State[S, Unit] =
    State(_ => ((), s))
}

object ex11 {
  import monads.Functor
  import monoids.Foldable
  // to be able to have Foldable extend Functor (thus, being a Foldable implies also being a Functor) 
  // we have to express map() in terms of foldLeft()/foldRight()
  trait FoldableFunctor[F[_]] extends Foldable[F] with Functor[F] {
    def foldRight[A, B](as: F[A])(z: B)(f: (A, B) => B): B = ???
    def foldLeft[A, B](as: F[A])(z: B)(f: (B, A) => B): B = ???

    def map[A, B](fa: F[A])(f: A => B): F[B] =
      foldRight(fa)(zero(): F[B])({ (a, b) => combi(f(a), b) })

    def combi[B](z: B, b: F[B]): F[B] = ???
    def zero[B](): F[B] = ???
  }
}