package fpinscala.parsing

import java.util.regex._
import scala.util.matching.Regex
import fpinscala.testing._
import fpinscala.testing.Prop._
import fpinscala.testing.SGen

trait Parsers[ParseError, Parser[+_]] { self => // so inner classes may call methods of trait
  case class Location(input: String, offset: Int = 0) {
    lazy val line = input.slice(0, offset + 1).count(_ == '\n') + 1
    lazy val col = input.slice(0, offset + 1).reverse.indexOf('\n')
  }

  def errorLocation(e: ParseError): Location
  def errorMessage(e: ParseError): String
  
  def scope[A](msg: String)(p: Parser[A]): Parser[A]
  def attempt[A](p: Parser[A]): Parser[A]

  def run[A](p: Parser[A])(input: String): Either[ParseError, A]

  // XXX: map is not yet fused on Parser
  def char(c: Char): Parser[Char] =
    map(string(c.toString)) { _.charAt(0) }

  def succeed[A](a: A): Parser[A] = unit(a)
  def unit[A](a: A): Parser[A]

  def string(s: String): Parser[String]

  // ex10: why not before ex9??? because flatMap->map is easier
  def map[A, B](a: Parser[A])(f: A => B): Parser[B] =
    flatMap(a)(a => succeed(f(a)))

  // ex9
  def product[A, B](s1: Parser[A], s2: => Parser[B]): Parser[(A, B)] =
    flatMap(s1)(a => map(s2) { b => (a, b) })

  implicit def regex(r: Regex): Parser[String]

  //ex7 -> f determines parser based on parsed input
  def flatMap[A, B](a: Parser[A])(f: A => Parser[B]): Parser[B]
  
  // ex15
   def seq[U,A,B](f: U => Parser[A])(g: A => Parser[B]): U => Parser[B] =
     u => flatMap(f(u))(g)
   

  // ex1
  def map2[A, B, C](s1: Parser[A], s2: => Parser[B])(f: (A, B) => C): Parser[C] =
    map(product(s1, s2))(f.tupled)

  def or[A](s1: Parser[A], s2: => Parser[A]): Parser[A]

  // ex4
  def listOfN[A](n: Int, p: Parser[A]): Parser[List[A]] =
    if (n == 0) succeed(Nil) else map2(p, listOfN(n - 1, p)) { _ :: _ }

  // ex3
  def many[A](p: Parser[A]): Parser[List[A]] =
    or(map2(p, many(p))(_ :: _), succeed(Nil)) // hopefully 'or' evaluates its left param first

  // ex1
  def many1[A](p: Parser[A]): Parser[List[A]] =
    map2(p, many(p))(_ :: _)

  // return portion of input string, that was examined
  def slice[A](p: Parser[A]): Parser[String]

  def axbx: Parser[(Int, Int)] =
    product(map(slice(many(char('a')))) { _.size }, map(slice(many(char('b')))) { _.size })

  case class ParserOps[A](p: Parser[A]) {
    def |[B >: A](p2: Parser[B]): Parser[B] = self.or(p, p2)
    def or[B >: A](p2: Parser[B]): Parser[B] = self.or(p, p2)
  }

  object Laws {
    import fpinscala.testing.ch8
    import fpinscala.testing.ch8.ex3
    import fpinscala.testing.ch8.ex3.Prop
    import fpinscala.testing.ch8.ex3.Prop._
    import fpinscala.testing.ch8.Gen
    import fpinscala.testing.ch8.SGen

    def equal[A](p1: Parser[A], p2: Parser[A])(in: Gen[String]): Prop =
      forAll(in) { s => run(p1)(s) == run(p2)(s) }

    // XXX: map is not yet fused on Parser (or use ParserOps?)
    def mapLaw[A](p: Parser[A])(in: Gen[String]): Prop = equal(p, map(p) { x => x })(in)

    // ex2
    def productAssocLaw[A](p1: Parser[A], p2: Parser[A], p3: Parser[A])(in: Gen[String]): Prop = {
      val left = map(product(product(p1, p2), p3)) { case ((x1, x2), x3) => (x1, x2, x3) }
      val right = map(product(p1, product(p2, p3))) { case (x1, (x2, x3)) => (x1, x2, x3) }
      equal(left, right)(in)
    }

    // ex6 -> yes, should be associative, because we're left-biased, so the order is always the same
    def orAssocLaw[A](p1: Parser[A], p2: Parser[A], p3: Parser[A])(in: Gen[String]): Prop = {
      val left = map(or(or(p1, p2), p3)) { case ((x1, x2), x3) => (x1, x2, x3) }
      val right = map(or(p1, or(p2, p3))) { case (x1, (x2, x3)) => (x1, x2, x3) }
      equal(left, right)(in)
    }

    def labelLaw[A](p: Parser[A], inputs: SGen[String]): Prop =
      forAll(inputs) { inputs =>
        sys.error("")
      }
    
    // ex16
    def seqAssocLaw[U,A,B,C](u: U, f: U => Parser[A], g: A => Parser[B], h: B => Parser[C])(in: Gen[String]): Prop = {
      val left = seq(f)(seq(g)(h))(u)
      val right = seq(seq(f)(g))(h)(u)
      equal(left, right)(in)
    }
    
    //ex17: seq(f)(id) == f
    def seqSucceed1[U,A,B,C](u: U, f: U => Parser[A])(in: Gen[String]): Prop = {
      val left = f(u)
      val right = seq(f)(unit(_))(u)
      equal(left, right)(in)
    }
    //ex17: seq(id)(f) == f
    def seqSucceed2[U,A,B,C](u: U, f: U => Parser[A])(in: Gen[String]): Prop = {
      val left = f(u)
      val right = seq(unit(_:U))(f)(u)
      equal(left, right)(in)
    }
    
    //ex18
    // x.append(Nil) == x
    // Nil.append(x) == x
  }
}

case class Location(input: String, offset: Int = 0) {

  lazy val line = input.slice(0, offset + 1).count(_ == '\n') + 1
  lazy val col = input.slice(0, offset + 1).reverse.indexOf('\n')

  def toError(msg: String): ParseError =
    ParseError(List((this, msg)))

  def advanceBy(n: Int) = copy(offset = offset + n)

  /* Returns the line corresponding to this location */
  def currentLine: String =
    if (input.length > 1) input.lines.drop(line - 1).next
    else ""
}

case class ParseError(stack: List[(Location, String)] = List(),
  otherFailures: List[ParseError] = List()) {
}