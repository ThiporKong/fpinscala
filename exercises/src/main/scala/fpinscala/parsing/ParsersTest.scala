package fpinscala.parsing

import scala.util.matching.Regex

object ParsersTest extends App {
  trait JSON
  
  object JSON {
    case object JNull extends JSON
    case class JNumber(get: Double) extends JSON
    case class JString(get: String) extends JSON
    case class JBool(get: Boolean) extends JSON
    case class JArray(get: IndexedSeq[JSON]) extends JSON
    case class JObject(get: Map[String, JSON]) extends JSON
  }

  case class Input(input: String, offset: Int = 0) {
    def text = input.substring(offset)
    def advance(delta: Int) = Input(input, offset + delta)
  }

  import fpinscala.state.State

  // a Parser is a collection of actions on an input state (state monad for Input)
  type MyParser[+A] = State[Input, Either[ParseError, A]]

  object MyParsers extends Parsers[ParseError, MyParser] {
    def run[A](p: MyParser[A])(input: String): Either[ParseError, A] =
      p.run(Input(input, 0))._1

    def unit[A](a: A) = State.unit(Right(a))

    def string(s: String): MyParser[String] =
      State.get.flatMap { input =>
        if (input.text.startsWith(s)) {
          State.set(input.advance(s.length)).map(_ => Right(s))
        } else {
          State.unit(Left(ParseError(List())))
        }
      }

    def regex(r: Regex): MyParser[String] =
      State.get.flatMap { input =>
        r.findPrefixOf(input.text) match {
          case Some(found) =>
            State.set(input.advance(found.length)).map(_ => Right(found))
          case None => State.unit(Left(ParseError(List())))
        }
      }

    def slice[A](p: MyParser[A]): MyParser[String] =
      State.get.flatMap { input =>
        p.flatMap {
          case Left(error) => State.unit(Left(error))
          case Right(_) => State.get.flatMap { inputAfter =>
            val slice = input.input.substring(input.offset, inputAfter.offset)
            State.unit(Right(slice))
          }
        }
      }

    def flatMap[A, B](a: MyParser[A])(f: A => MyParser[B]): MyParser[B] = {
      a.flatMap {
        case Left(error) => State.unit(Left(error))
        case Right(a) => f(a)
      }
    }

    def or[A](s1: MyParser[A], s2: => MyParser[A]): MyParser[A] =
      s1.flatMap {
        case Left(error) => s2
        case ok1 => State.unit(ok1)
      }

    def scope[A](msg: String)(p: MyParser[A]): MyParser[A] = ???
    def attempt[A](p: MyParser[A]): MyParser[A] = ???

    def errorLocation(e: ParseError): Location = ???
    def errorMessage(e: ParseError): String = ???
  }

  // XXX the ParseError is missing
  def jsonParser[Parser[+_]](P: Parsers[ParseError, Parser]): Parser[JSON] = {
    import P._
    val spaces = slice(many(char(' ')))

    val nullParser = flatMap(string("null"))(_ => succeed(JSON.JNull))
    val numberParser = flatMap(regex("\\d+".r))(s => succeed(JSON.JNumber(s.toDouble)))
    val boolParser = flatMap(regex("true|false".r))(s => succeed(JSON.JBool(java.lang.Boolean.getBoolean(s))))

    val arrayOpenBracket = string("[")
    val arrayCloseBracket = string("]")
    val arrayCommaBracket = string(",")

    lazy val arrayParser: Parser[JSON.JArray] = {
      val values = many(product(arrayCommaBracket, arrayParser))
      val all = product(arrayOpenBracket, product(values, arrayCloseBracket))
      flatMap(all)(_ => succeed(JSON.JArray(Array[JSON]())))
    }

    val objectParser = succeed(JSON.JObject(Map[String, JSON]()))
    val objectOpenBracket = string("{")
    val objectCloseBracket = string("}")

    val valueParser =
      List[Parser[JSON]](
        objectParser,
        arrayParser,
        boolParser,
        numberParser).foldLeft(nullParser: Parser[JSON])((a, b) => or(a, b))

    //valueParser
    arrayParser
  }

  val p = jsonParser(MyParsers)
  import MyParsers._
  val nullParser = string("null")
  val zombieParser = string("zombie")
  System.out.println("RESULT:", many(or(nullParser, zombieParser)).run(Input("nullzombienullzombiezombie")))
  System.out.println("RESULT:", many(nullParser).run(Input("")))
  System.out.println("RESULT:", many(nullParser).run(Input("nullnullnull")))
  System.out.println("RESULT:", many(nullParser).run(Input("nullnollnull")))
  System.out.println("RESULT:", many1(nullParser).run(Input("nullnollnull")))
  System.out.println("RESULT:", many1(nullParser).run(Input("nollnollnull")))
  System.out.println("RESULT:", axbx.run(Input("aaabbbbbb")))
}