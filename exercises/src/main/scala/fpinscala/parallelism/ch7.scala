package fpinscala.parallelism.ch7

import scala.util.Random

object Par extends App {
  import java.util.concurrent.Future
  import java.util.concurrent.ExecutorService
  import java.util.concurrent.TimeUnit
  import java.util.concurrent.Callable
  import java.util.concurrent.Executors

  type Par[A] = ExecutorService => Future[A];

  // ex5
  object ex5 {
  def product[A,B](fa: Par[A], fb: Par[B]): Par[(A,B)] =
    ex => unit(fa(ex).get,fb(ex).get)(ex)
    
  def map[A,B](fa: Par[A])(f: A => B): Par[B] = 
    ex => unit(f(fa(ex).get))(ex)
  
    // why do I need the double parens?
  def map2[A, B, C](a: Par[A], b: Par[B])(f: (A, B) => C): Par[C] =
    map(product(a,b)){case ((a,b)) => f(a,b)}
  }
    
  // ex1
  // ex3
  def map2[A, B, C](a: Par[A], b: Par[B])(f: (A, B) => C): Par[C] = ex => unit(f(a(ex).get, b(ex).get))(ex)
  def map[A,B](fa: Par[A])(f: A => B): Par[B] = map2(fa, unit(()))((a,_) => f(a))
  
  // ex6
  def parMap[A,B](l: List[A])(f: A => B): Par[List[B]] =
    fork {
        val lbs = l.map { asyncF(f) }
        println("parMap: " + lbs)
    sequence(lbs)
  }

  // ex7
  def sequence[A](l: List[Par[A]]): Par[List[A]] =
    ex => unit(l.map { pa => pa(ex) }.map { _.get })(ex)
  //  l.foldRight(unit(Nil:List[A])) { map2(_, _)(_ :: _)}
    
  
  // ex8
  def parFilter[A](l: List[A])(f: A => Boolean): Par[List[A]] =
	  map(parMap(l){a => if (f(a)) Some(a) else None}){_.flatten}
    
  // ex3
  def unit[A](a: A): Par[A] =
    ex => new Future[A] {
      def cancel(mayInterruptIfRunning: Boolean) = false;
      def isCancelled = false
      def isDone = true
      def get: A = a
      def get(timeout: Long, unit: TimeUnit) = a
    }

  // ex3
  def fork[A](a: => Par[A]): Par[A] = {
    ex => ex.submit(new Callable[A] {
      def call: A = a(ex).get
    })
  }

  def async[A](a: => A): Par[A] = fork(unit(a))
  
  // ex4
  def asyncF[A,B](f: A => B): A => Par[B] = a => async(f(a))

  def run[A](ex: ExecutorService)(a: Par[A]): Future[A] = a(ex)

  //def map2[A, B, C](a: Par[A], b: Par[B])(f: (A, B) => C): Par[C]

  //ex2
  // EXERCISE 2: Before continuing, try to come up with representations for Par
  // and Strategy that make it possible to implement the functions of our API.
  // What do they mean with Strategy???
  
  def sortPar[A](l: Par[List[A]])(implicit ord: Ordering[A]): Par[List[A]] =
    map(l)(_.sorted)
    
//EXERCISE 9 (hard, optional): Given map(y)(id) == y, it is a free
//theorem that map(map(y)(g))(f) == map(y)(f compose g). (This is
//sometimes called map fusion, and it can be used as an optimization—rather than
//spawning a separate parallel computation to compute the second mapping, we can
//fold it into the first mapping.)
  
//  EXERCISE 10 (hard, optional): Take a look through the various static methods
//in Executors to get a feel for the different implementations of
//ExecutorService that exist.
  
//  EXERCISE 11 (hard, optional): Can you show that any fixed size thread pool
//can be made to deadlock given this implementation of fork?
  
//  EXERCISE 12 (hard, optional): Can you figure out a way to still evaluate fa in
//a separate logical thread, but avoid deadlock?
  
//  EXERCISE 13: Can you think of other laws that should hold for your
//implementation of unit, fork, and map2? Do any of them have interesting
//consequences?
  
//  EXERCISE 14: Try writing a function to choose between two forking
//computations based on the result of an initial computation. Can this be
//implemented in terms of existing combinators or is a new primitive required?
  def choice[A](a: Par[Boolean])(ifTrue: Par[A], ifFalse: Par[A]): Par[A] =
      chooser(a)(if (_) ifTrue else ifFalse)
  
  // ex15
  def choiceN[A](a: Par[Int])(choices: List[Par[A]]): Par[A] =
      chooser(a)(choices)
  
  // ex16
  def choiceMap[A,B](a: Par[A])(choices: Map[A,Par[B]]): Par[B] =
      chooser(a)(choices)
      
  // ex17
  def chooser[A,B](a: Par[A])(choices: A => Par[B]): Par[B] =
      ex => choices(a(ex).get)(ex)
      
  def flatMap[A,B](a: Par[A])(f: A => Par[B]): Par[B] = chooser(a)(f)
      
  // ex18
  def join[A](a: Par[Par[A]]): Par[A] = flatMap(a){ x => x }
}