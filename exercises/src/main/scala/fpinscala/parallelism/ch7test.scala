package fpinscala.parallelism

import java.util.concurrent.Executors
import ch7.Par._

object ch7test extends App {
  def sum(as: IndexedSeq[Int]): Par[Int] = {
    println(Thread.currentThread().getName() + ": sum(%s)".format(as.size))
    if (as.size <= 1) {
      unit(as.headOption getOrElse 0)
    } else {
      val (l, r) = as.splitAt(as.length / 2)
      map2(fork(sum(l)), fork(sum(r)))(_ + _)
    }
  }

  // use unbounded thread pool
  val threadPool = Executors.newCachedThreadPool()
  try {
    def test1 = run(threadPool)(sum(Array.fill(1000)(1))).get()
    println(test1)
  } finally {
    threadPool.shutdown()
  }
}