package fpinscala.monoids

trait Monoid[A] {
  def op(a1: A, a2: A): A
  def zero: A
}

object Monoid {

  val stringMonoid = new Monoid[String] {
    def op(a1: String, a2: String) = a1 + a2
    val zero = ""
  }

  def listMonoid[A] = new Monoid[List[A]] {
    def op(a1: List[A], a2: List[A]) = a1 ++ a2
    val zero = Nil
  }

  //ex1
  val intAddition: Monoid[Int] = new Monoid[Int] {
    def op(a1: Int, a2: Int) = a1 + a2
    val zero = 0
  }

  val intMultiplication: Monoid[Int] = new Monoid[Int] {
    def op(a1: Int, a2: Int) = a1 * a2
    val zero = 1
  }

  val booleanOr: Monoid[Boolean] = new Monoid[Boolean] {
    def op(a1: Boolean, a2: Boolean) = a1 | a2
    val zero = false
  }

  val booleanAnd: Monoid[Boolean] = new Monoid[Boolean] {
    def op(a1: Boolean, a2: Boolean) = a1 & a2
    val zero = true
  }

  //ex2
  // not commutative!
  def optionMonoid[A]: Monoid[Option[A]] = new Monoid[Option[A]] {
    def op(a1: Option[A], a2: Option[A]) = a1.orElse(a2)
    val zero = None
  }

  //ex3
  // not commutative!
  def endoMonoid[A]: Monoid[A => A] = new Monoid[A => A] {
    def op(a1: A => A, a2: A => A) = a1.andThen(a2)
    val zero = identity[A] _
  }

  import fpinscala.testing.ch8
  import fpinscala.testing.ch8.ex3
  import fpinscala.testing.ch8.ex3.Prop
  import fpinscala.testing.ch8.ex3.Prop._
  import fpinscala.testing.ch8.Gen
  import fpinscala.testing.ch8.SGen

  // ex4
  def monoidLaws[A](m: Monoid[A], gen: Gen[A]): Prop =
    forAll(gen) { a =>
      m.op(a, m.zero) == a
    } &&
      forAll(gen) { a =>
        m.op(m.zero, a) == a
      }

  // ex5: XX Why does it take a string to construct the monoid? wordsMonoid => trimMonoid?
  def wordsMonoid(s: String): Monoid[String] = new Monoid[String] {
    def op(a1: String, a2: String) = a1.trim + " " + a2.trim
    val zero = ""
  }

  def trimMonoid(s: String): Monoid[String] = sys.error("todo")

  // ex6
  def concatenate[A](as: List[A], m: Monoid[A]): A =
    as.foldRight(m.zero)(m.op)

  // ex7
  def foldMap[A, B](as: List[A], m: Monoid[B])(f: A => B): B =
    concatenate(as.map(f), m)

  // ex8: TRICKY: f: A => (B => B)
  def foldRight[A, B](as: List[A])(z: B)(f: (A, B) => B): B =
    foldMap(as, endoMonoid[B])(f.curried)(z)

  def foldLeft[A, B](as: List[A])(z: B)(f: (B, A) => B): B =
    foldMap(as, endoMonoid[B])({ a => b => f(b, a) })(z)

  // ex11
  def foldMapV[A, B](as: IndexedSeq[A], m: Monoid[B])(f: A => B): B =
    as.size match {
      case 0 => m.zero
      case 1 => f(as.head)
      case 2 => m.op(f(as.head), f(as.tail.head))
      case _ =>
        val mid = (as.size / 2) max 1
        m.op(foldMapV(as.slice(0, mid), m)(f), foldMapV(as.slice(mid, as.size), m)(f))
    }

  //ex12
  def ordered(ints: IndexedSeq[Int]): Boolean =
    foldMapV(ints.sliding(2).toIndexedSeq, booleanAnd)(it => it.head <= it.tail.head)

  sealed trait WC
  case class Stub(chars: String) extends WC
  case class Part(lStub: String, words: Int, rStub: String) extends WC

  // ex9
  lazy val wcMonoid: Monoid[WC] = new Monoid[WC] {
    def op(a1: WC, a2: WC) = (a1, a2) match {
      case (Part(lStub, words, x1), Part(x2, words2, rStub2)) => Part(lStub, words + words2 + ((x1 + x2).size min 1), rStub2)
      case (Stub(chars1), Part(lStub, words, rStub)) => Part(chars1 + lStub, words, rStub)
      case (Part(lStub, words, rStub), Stub(chars)) => Part(lStub, words, rStub + chars)
      case (Stub(chars1), Stub(chars2)) => Stub(chars1 + chars2)
    }
    val zero = Stub("")
  }

  //ex10
  def count(s: String): Int = {
    def loop(s: String): List[WC] =
      if (s.size <= 1) {
        if (s.trim.isEmpty) {
          List(Part("", 0, ""))
        } else {
          List(Stub(s))
        }
      } else {
        val mid = (s.size / 2) min 1
        loop(s.substring(0, mid)) ++ loop(s.substring(mid))
      }

    loop(s).foldRight(wcMonoid.zero)(wcMonoid.op) match {
      case Part(left, count, right) => count + (left.size min 1) + (right.size min 1)
      case Stub(stub) => (stub.size min 1)
    }
  }

  //17
  def productMonoid[A, B](A: Monoid[A], B: Monoid[B]): Monoid[(A, B)] = new Monoid[(A, B)] {
    def op(x: (A, B), y: (A, B)) = (A.op(x._1, y._1), B.op(y._2, y._2))
    val zero = (A.zero, B.zero)
  }

  //ex18
  //  def coproductMonoid[A, B](A: Monoid[A], B: Monoid[B]): Monoid[Either[A, B]] = new Monoid[Either[A,B]] {
  //  }

  //ex19
  def functionMonoid[A, B](B: Monoid[B]): Monoid[A => B] = new Monoid[A => B] {
    def op(f: A => B, g: A => B) = a => B.op(f(a), g(a))
    val zero: A => B = _ => B.zero
  }

  def mapMergeMonoid[K, V](V: Monoid[V]): Monoid[Map[K, V]] = new Monoid[Map[K, V]] {
    def op(f: Map[K, V], g: Map[K, V]) =
      f ++ g.map { case (k,v) => (k -> V.op(v, f.getOrElse(k, V.zero))) }
    val zero = Map[K, V]()
  }

  def bag[A](as: IndexedSeq[A]): Map[A, Int] =
    sys.error("todo")

  // XXX ex20 Frequency Map no code??
  def frequencyMap(strings: IndexedSeq[String]): Map[String, Int] =
    foldMapV(strings, mapMergeMonoid[String, Int](intAddition)) { words =>
      foldMapV(words.split(" +"), mapMergeMonoid[String, Int](intAddition)) { word => Map(word -> 1) }
    }
}

//ex13
trait Foldable[F[_]] {
  import Monoid._

  def foldRight[A, B](as: F[A])(z: B)(f: (A, B) => B): B

  def foldLeft[A, B](as: F[A])(z: B)(f: (B, A) => B): B

  def foldMap[A, B](as: F[A])(f: A => B)(mb: Monoid[B]): B =
    foldRight(as)(mb.zero)((a, b) => mb.op(f(a), b))

  def concatenate[A](as: F[A])(m: Monoid[A]): A =
    foldMap(as)(identity)(m)

  // ex16
  def toList[A](as: F[A]): List[A] =
    foldMap(as)(List(_))(listMonoid)
}

object ListFoldable extends Foldable[List] {
  override def foldRight[A, B](as: List[A])(z: B)(f: (A, B) => B) = as.foldRight(z)(f)
  override def foldLeft[A, B](as: List[A])(z: B)(f: (B, A) => B) = as.foldLeft(z)(f)
}

object IndexedSeqFoldable extends Foldable[IndexedSeq] {
  override def foldRight[A, B](as: IndexedSeq[A])(z: B)(f: (A, B) => B) = as.foldRight(z)(f)
  override def foldLeft[A, B](as: IndexedSeq[A])(z: B)(f: (B, A) => B) = as.foldLeft(z)(f)
}

object StreamFoldable extends Foldable[Stream] {
  override def foldRight[A, B](as: Stream[A])(z: B)(f: (A, B) => B) = as.foldRight(z)(f)
  override def foldLeft[A, B](as: Stream[A])(z: B)(f: (B, A) => B) = as.foldLeft(z)(f)
}

sealed trait Tree[+A]
case class Leaf[A](value: A) extends Tree[A]
case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]

//ex14
object TreeFoldable extends Foldable[Tree] {
  override def foldLeft[A, B](as: Tree[A])(z: B)(f: (B, A) => B) =
    as match {
      case Leaf(value) => f(z, value)
      case Branch(left, right) => foldLeft(right)(foldLeft(left)(z)(f))(f)
    }

  override def foldRight[A, B](as: Tree[A])(z: B)(f: (A, B) => B) =
    as match {
      case Leaf(value) => f(value, z)
      case Branch(left, right) => foldRight(left)(foldRight(right)(z)(f))(f)
    }
}

// ex15
object OptionFoldable extends Foldable[Option] {
  override def foldLeft[A, B](as: Option[A])(z: B)(f: (B, A) => B) =
    as match {
      case Some(v) => f(z, v)
      case None => z
    }

  override def foldRight[A, B](as: Option[A])(z: B)(f: (A, B) => B) =
    as match {
      case Some(v) => f(v, z)
      case None => z
    }
}