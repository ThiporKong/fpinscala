package fpinscala.monoids

import Monoid._
import fpinscala.testing.ch8
import fpinscala.testing.ch8.ex3
import fpinscala.testing.ch8.ex3.Prop
import fpinscala.testing.ch8.ex3.Prop._
import fpinscala.testing.ch8.Gen
import fpinscala.testing.ch8.SGen
import fpinscala.state.RNG
import scala.concurrent.duration.IntMult

object MonoidTest extends App {
  val rng = RNG.simple(4711)
  if (false) {
    println(monoidLaws(intAddition, ch8.ex5.choose(-10, 10)).run(10, 40, rng))
    println(monoidLaws(intMultiplication, ch8.ex5.choose(-10, 10)).run(10, 40, rng))
    println(monoidLaws(booleanOr, ch8.ex5.boolean).run(10, 40, rng))
    // no SGen supported
    println(monoidLaws(listMonoid[Int], ch8.ex5.listOfN(3, ch8.ex5.choose(-10, 10))).run(10, 40, rng))
  }

  if (false) {
    val op = wordsMonoid("").op _
    println(op("Hic", op("est ", "chorda ")))
  }

  if (false) {
    List(
      "Das ist ein Test",
      "Das ist ein Test huhuh     sfa    ",
      "",
      "   ",
      "x  ",
      "    x",
      "x x ",
      "  x x").map { text => (text, count(text)) }.foreach { println _ }
  }

  if (false) {
    println(foldMapV(Array.fill(1000)(1), intAddition)(identity))
  }
  
  if (false) {
        List(
            Array(1,2,3,4,5,6),
            Array(1,2,3,4,0,6)
      ).map { x => (x, ordered(x)) }.foreach { println _ }
  }
  
  if (false) {
    println(IndexedSeqFoldable.toList(Array(1,2,3,4,5)))
  }
  
  if (true) {
    println(frequencyMap(Array("Ich Du Der", "Der Die", "Die basfa", "Ich", "Ich sadf", " asfdas sadf Ich")))
  }
}