package fpinscala.errorhandling

sealed trait Either[+E, +A] {
  def map[B](f: A => B): Either[E, B]
  def flatMap[EE >: E, B](f: A => Either[EE, B]): Either[EE, B]
  def orElse[EE >: E, B >: A](b: => Either[EE, B]): Either[EE, B]
  def map2[EE >: E, B, C](b: Either[EE, B])(f: (A, B) => C): Either[EE, C]
}

case class Left[+E](get: Seq[E]) extends Either[E, Nothing] {
  override def map[B](f: Nothing => B): Either[E, B] = this
  override def flatMap[EE >: E, B](f: Nothing => Either[EE, B]): Either[EE, B] = this
  override def orElse[EE >: E, B](b: => Either[EE, B]): Either[EE, B] = map2(b)((_, x) => x)
  override def map2[EE >: E, B, C](b: Either[EE, B])(f: (Nothing, B) => C): Either[EE, C] = b match {
    case Left(bv) => Left(get ++ bv)
    case Right(_) => this
  }
}

case class Right[+A](get: A) extends Either[Nothing, A] {
  override def map[B](f: A => B): Either[Nothing, B] = Right(f(get))
  override def flatMap[EE, B](f: A => Either[EE, B]): Either[EE, B] = f(get)
  override def orElse[EE, B >: A](b: => Either[EE, B]): Either[EE, B] = this
  override def map2[EE, B, C](b: Either[EE, B])(f: (A, B) => C): Either[EE, C] = for (bv <- b) yield { f(get, bv) }
}

object Either {
  def mean(xs: IndexedSeq[Double]): Either[String, Double] =
    if (xs.isEmpty)
      Left(Seq("mean of empty list!"))
    else
      Right(xs.sum / xs.length)

  def safeDiv(x: Double, y: Double): Either[Exception, Double] =
    try {
      Right(x / y)
    } catch {
      case e: Exception => Left(Seq(e))
    }

  object ex7 {
    println(for {
      age <- Right(42)
      name <- Left("invalid name")
      salary <- Right(1000000.0)
    } yield (name, age, salary))
  }

  // ex8
  def traverse[A, B, L](a: List[A])(f: A => Either[L, B]): Either[L, List[B]] = a match {
    case hd :: tl => for (hd1 <- f(hd); tl1 <- traverse(tl)(f)) yield { hd1 :: tl1 }
    case Nil => Right(Nil)
  }
  def sequence[A, L](a: List[Either[L, A]]): Either[L, List[A]] = traverse(a)(x => x)

  // ex9
  object ex9 {
    // would implement a new datatype that does merging on the Left side
    // see type definitions

    case class Person(name: Name, age: Age)
    sealed class Name(val value: String)
    sealed class Age(val value: Int)
    def mkName(name: String): Either[String, Name] =
      if (name == "" || name == null) Left(Seq("Name is empty."))
      else Right(new Name(name))
    def mkAge(age: Int): Either[String, Age] =
      if (age < 0) Left(Seq("Age is out of range."))
      else Right(new Age(age))
    def mkPerson(name: String, age: Int): Either[String, Person] =
      mkName(name).map2(mkAge(age))(Person(_, _))

    mkPerson("", -1)
    Left(Seq("Hallo")).orElse(Left(Seq("Blubber")))
  }
}